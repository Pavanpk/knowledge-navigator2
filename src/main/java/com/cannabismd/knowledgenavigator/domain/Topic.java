package com.cannabismd.knowledgenavigator.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class Topic implements Serializable {

    private String topicId;
    private String topicName;
    private String topicQuery;
    private boolean isActive;
}
