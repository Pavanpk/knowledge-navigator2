package com.cannabismd.knowledgenavigator.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
public class Organization implements Serializable {

    private String orgId;
    private String orgDisplayName;
    private boolean isActive;
    private List<Topic> topics;
}
