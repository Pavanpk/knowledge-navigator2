package com.cannabismd.knowledgenavigator.endpoint;

import com.cannabismd.knowledgenavigator.domain.Organization;
import com.cannabismd.knowledgenavigator.domain.Topic;
import com.cannabismd.knowledgenavigator.service.OrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/organization")
public class OrganizationRestController {

    @Autowired
    private OrganizationService organizationService;

    @GetMapping("/{orgId}")
    public Organization getOrganizationConfig(@PathVariable String orgId) {
        return organizationService.findOrganizationConfigByOrgId(orgId);

    }

    @GetMapping("/{orgId}/topics")
    public List<Topic> getTopics(@PathVariable String orgId) {
        return null;
    }

}
