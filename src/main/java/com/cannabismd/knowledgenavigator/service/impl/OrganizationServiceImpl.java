package com.cannabismd.knowledgenavigator.service.impl;

import com.cannabismd.knowledgenavigator.domain.Organization;
import com.cannabismd.knowledgenavigator.domain.Topic;
import com.cannabismd.knowledgenavigator.integration.KnowledgeNavigatorConfigRestClient;
import com.cannabismd.knowledgenavigator.service.OrganizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrganizationServiceImpl implements OrganizationService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private KnowledgeNavigatorConfigRestClient knowledgeNavigatorConfigRestClient;

    @Override
    public Organization findOrganizationConfigByOrgId(String orgId) {
        logger.debug("findOrganizationConfigByOrgId - {}", orgId);
        return knowledgeNavigatorConfigRestClient.getOrganizationConfig(orgId);
    }

    @Override
    public List<Topic> findTopicsByOrgId(String orgId) {
        logger.debug("findTopicsByOrgId - {}", orgId);
        return null;

    }

    @Override
    public String getTopicsByTopicId() {
        return null;
    }
}
