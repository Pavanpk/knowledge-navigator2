package com.cannabismd.knowledgenavigator.service;

import com.cannabismd.knowledgenavigator.domain.Organization;
import com.cannabismd.knowledgenavigator.domain.Topic;

import java.util.List;

public interface OrganizationService {

    Organization findOrganizationConfigByOrgId(String orgId);

    List<Topic> findTopicsByOrgId(String orgId);

    String getTopicsByTopicId();

}
