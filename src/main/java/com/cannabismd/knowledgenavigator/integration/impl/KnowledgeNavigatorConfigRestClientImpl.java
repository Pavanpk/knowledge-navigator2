package com.cannabismd.knowledgenavigator.integration.impl;

import com.cannabismd.knowledgenavigator.domain.Organization;
import com.cannabismd.knowledgenavigator.integration.KnowledgeNavigatorConfigRestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class KnowledgeNavigatorConfigRestClientImpl implements KnowledgeNavigatorConfigRestClient {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${knconfig.endpoint.uri}")
    private String knconfigEndpoint;

    public Organization getOrganizationConfig(String orgId) {
        return restTemplate.getForObject(knconfigEndpoint + "/admin/organization/" + orgId, Organization.class);
    }
}
