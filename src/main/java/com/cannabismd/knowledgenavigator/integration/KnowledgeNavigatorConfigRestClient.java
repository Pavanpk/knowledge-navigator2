package com.cannabismd.knowledgenavigator.integration;

import com.cannabismd.knowledgenavigator.domain.Organization;

public interface KnowledgeNavigatorConfigRestClient {

    public Organization getOrganizationConfig(String orgId);
}
