import React, { PureComponent } from "react";
import Login from "./Login/Login";
import { Route } from "react-router-dom";
// import { connect } from "react-redux";
import Spinner from "../../components/ui/Spinners/Spinner";
import { Button, Message } from "semantic-ui-react";
import Input from "../../components/ui/Input/Input";

//authentication redux / store / firebase
// import * as actions from "../../store/actions/index";

class Authentication extends PureComponent {
  state = {
    controls: {
      email: {
        elementType: "input",
        elementConfig: {
          type: "email",
          placeholder: "john@example.com"
        },
        value: "",
        validation: {
          required: true,
          isEmail: true
        },
        valid: false,
        touched: false
      },
      password: {
        elementType: "input",
        elementConfig: {
          type: "password",
          placeholder: "**********"
        },
        value: "",
        validation: {
          required: true,
          minLength: 6
        },
        valid: false,
        touched: false
      }
    },
    isSignup: false,
    forgetpass: false
  };

  checkValidity(value, rules) {
    let isValid = true;
    if (!rules) {
      return true;
    }

    if (rules.required) {
      isValid = value.trim() !== "" && isValid;
    }

    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid;
    }

    if (rules.maxLength) {
      isValid = value.length <= rules.maxLength && isValid;
    }

    if (rules.isEmail) {
      const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
      isValid = pattern.test(value) && isValid;
    }

    if (rules.isNumeric) {
      const pattern = /^\d+$/;
      isValid = pattern.test(value) && isValid;
    }

    return isValid;
  }

  inputChangedHandler = (event, controlName) => {
    const updatedControls = {
      ...this.state.controls,
      [controlName]: {
        ...this.state.controls[controlName],
        value: event.target.value,
        valid: this.checkValidity(
          event.target.value,
          this.state.controls[controlName].validation
        ),
        touched: true
      }
    };
    this.setState({ controls: updatedControls });
  };

  submitHandler = event => {
    event.preventDefault();
    this.props.onAuth(
      this.state.controls.email.value,
      this.state.controls.password.value,
      this.state.isSignup
    );
  };

  switchAuthModeHandler = e => {
    e.preventDefault();
    this.setState(prevState => {
      return { isSignup: !prevState.isSignup };
    });
  };

  render() {
    const formElementsArray = [];
    for (let key in this.state.controls) {
      formElementsArray.push({
        id: key,
        config: this.state.controls[key]
      });
    }

    let form = formElementsArray.map(formElement => (
      <Input
        key={formElement.id}
        elementType={formElement.config.elementType}
        elementConfig={formElement.config.elementConfig}
        value={formElement.config.value}
        invalid={!formElement.config.invalid}
        shouldValidate={formElement.config.shouldValidate}
        touched={formElement.config.touched}
        changed={event => this.inputChangedHandler(event, formElement.id)}
      />
    ));

    let emailverify = [
      <h2>Getting trouble?</h2>,
      <p>Just reset password via email</p>,
      form[0],
      <Button fluid color="blue">
        Verify
      </Button>,
      <Message>
        Still have a problem?
        <a href="mailto:park@fatfiahmarketing.com"> Click here</a>
        <br />
        Go back to log in page
        <a
          style={{ cursor: "pointer" }}
          href="/"
          onClick={e => {
            e.preventDefault();
            this.setState({ forgetpass: false });
          }}
        >
          Sign in
        </a>
      </Message>
    ];

    if (this.props.loading) {
      form = <Spinner />;
    }

    let errorMessage = null;
    if (this.props.error) {
      errorMessage = <p>{this.props.error.message}</p>;
    }

    if (this.props.auth === true) {
      return <Route path="/" component={<p>Dashboard</p>} />;
    } else {
      return (
        <Login>
          <div className="Auth">
            {this.state.forgetpass === true ? (
              emailverify
            ) : (
              <React.Fragment>
                <form onSubmit={this.submitHandler}>
                  <h2>Knowledge Navigator</h2>
                  <p>Sign in to your account and enjoy unlimited perks.</p>

                  {form}
                  <Button fluid color="blue">
                    {this.state.isSignup ? "Sign Up" : "Log In"}
                  </Button>
                </form>
                {errorMessage && (
                  <Message negative>
                    <Message.Header>
                      Sorry, your login processing failed
                    </Message.Header>
                    <p>{errorMessage}</p>
                  </Message>
                )}

                <Message>
                  <p>
                    Having trouble logging in?
                    <a
                      href="/"
                      style={{ cursor: "pointer" }}
                      onClick={e => {
                        e.preventDefault();
                        this.setState({ forgetpass: !this.state.forgetpass });
                      }}
                    >
                      Verify by email
                    </a>
                  </p>
                  {this.state.isSignup ? (
                    <p>
                      Have an account already?
                      <a href="/" onClick={this.switchAuthModeHandler}>
                        Sign in
                      </a>
                    </p>
                  ) : (
                    <p>
                      Do you have an account yet?
                      <a href="/" onClick={this.switchAuthModeHandler}>
                        Sign up
                      </a>
                    </p>
                  )}
                </Message>
              </React.Fragment>
            )}
          </div>
        </Login>
      );
    }
  }
}

export default Authentication;
