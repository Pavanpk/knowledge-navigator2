import React from "react";
import LoginPageCover from "./LoginPageCover/LoginPageCover";
import { Button, Message } from "semantic-ui-react";
import Input from "../../components/ui/Input/Input";

// Rule: java base authentication
// useEffect: check authed
// if ( user logined -> go to dashboard)
// if ( user not logined -> show 'login' / 'signup' / 'forgot pass')

const LoginPage = ({ setAuthed }) => {
  const [isSignup, setisSignup] = React.useState(false);
  const [forgetpass, setForgetpass] = React.useState(false);

  // form validation : java oAuth
  // const checkValidity = () => {};

  const inputChangedHandler = (event, id) => {
    // e.target.value
    // this.setState({...controls, {id: e.target.value}})
  };

  const submitHandler = () => {};
  const switchAuthModeHandler = e => {
    e.preventDefault();
    setisSignup(!isSignup);
  };

  let constrols = {
    email: {
      elementType: "input",
      elementConfig: {
        type: "email",
        placeholder: "john@example.com"
      },
      value: "",
      validation: {
        required: true,
        isEmail: true
      },
      valid: false,
      touched: false
    },
    password: {
      elementType: "input",
      elementConfig: {
        type: "password",
        placeholder: "**********"
      },
      value: "",
      validation: {
        required: true,
        minLength: 6
      },
      valid: false,
      touched: false
    }
  };

  const formElementArray = [];
  for (let key in constrols) {
    formElementArray.push({
      id: key,
      config: constrols[key].elementConfig
    });
    //console.log("constrols[key]", constrols[key]);
  }

  let form = formElementArray.map(item => (
    <Input
      key={item.id}
      elementType={item.config.elementType}
      elementConfig={item.config.elementConfig}
      value={item.config.value}
      invalid={!item.config.invalid}
      shouldValidate={item.config.shouldValidate}
      touched={item.config.touched}
      changed={event => inputChangedHandler(event, item.id)}
    />
  ));

  let ForgotPass = (
    <React.Fragment>
      <h2>Getting trouble?</h2>
      <p>Just reset password via email</p> {form[0]}
      <Button fluid color="blue">
        Verify
      </Button>
      <Message>
        <p className="Justify">
          Still have a problem?
          <a href="mailto:park@fatfiahmarketing.com"> Click here</a>
        </p>
        <p className="Justify">
          Go back to log in page
          <a
            style={{ cursor: "pointer" }}
            href="/"
            onClick={e => {
              e.preventDefault();
              setForgetpass(!forgetpass);
            }}
          >
            Sign in
          </a>
        </p>
      </Message>
    </React.Fragment>
  );

  const errorMessage = "";

  let Login = (
    <React.Fragment>
      <form onSubmit={submitHandler}>
        <h2>Knowledge Navigator</h2>
        <p>Sign in to your account and enjoy unlimited perks.</p>

        {form}
        <Button fluid color="blue" onClick={() => setAuthed(true)}>
          {isSignup ? "Sign Up" : "Log In"}
        </Button>
      </form>
      {errorMessage && (
        <Message negative>
          <Message.Header>Sorry, your login processing failed</Message.Header>
          <p>{errorMessage}</p>
        </Message>
      )}

      <Message>
        <p className="Justify">
          Having trouble logging in?{" "}
          <a
            href="/"
            style={{ cursor: "pointer" }}
            onClick={e => {
              e.preventDefault();
              setForgetpass(!forgetpass);
            }}
          >
            Verify by email
          </a>
        </p>
        {isSignup ? (
          <p className="Justify">
            Have an account already?
            <a href="/" onClick={switchAuthModeHandler}>
              Sign in
            </a>
          </p>
        ) : (
          <p className="Justify">
            Do you have an account yet?
            <a href="/" onClick={switchAuthModeHandler}>
              Sign up
            </a>
          </p>
        )}
      </Message>
    </React.Fragment>
  );

  return <LoginPageCover>{forgetpass ? ForgotPass : Login}</LoginPageCover>;
};

export default LoginPage;
