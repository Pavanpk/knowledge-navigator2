import React from "react";
import { Grid, Container } from "semantic-ui-react";
import Carousel from "../../../components/ui/Carousel/Carousel";

import "./LoginPageCover.scss";

const LoginPageCover = props => {
  const contents = {
    title: "What is Knowledge Navigator?",
    sub: "Empower your strategies with data & AI",
    desc:
      "The Knowledge Navigator gives analytics picked up from Million’s of URLS using industry leading online & social listening techniques that can be filtered to niche demographics to suit any campaign."
  };

  return (
    <Grid className="LoginPageCover">
      <Grid.Column mobile={16} tablet={8} computer={6} style={{ padding: 0 }}>
        <Carousel
          title={contents.title}
          sub={contents.sub}
          desc={contents.desc}
          className="Carousel"
        />
      </Grid.Column>

      <Grid.Column
        mobile={16}
        tablet={8}
        computer={10}
        style={{
          height: "100vh",
          display: "flex",
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <Container
          className="Inner"
          style={{ minWidth: "400px", width: "40%" }}
        >
          {props.children}
        </Container>
      </Grid.Column>
    </Grid>
  );
};

export default LoginPageCover;
