import React from "react";
import CardBox from "../../ui/CardBox/CardBox";
import { Grid, Card } from "semantic-ui-react";
import ResultTable from "../../ui/ResultTable/ResultTable";

// 1 row: Top Occupation
// card for "", "", "", ""

const TopOccupation = () => {
  const _title = [
    "Most Active Author",
    "Most Influential Author",
    "Most Active Company",
    "Most Influential Company"
  ];

  const cardList = _title.map(item => {
    return (
      <Grid.Column mobile={8} tablet={8} computer={4} key={item}>
        <CardBox title={item}></CardBox>
      </Grid.Column>
    );
  });

  return (
    <Grid.Row centered columns={4}>
      {cardList}
    </Grid.Row>
  );
};

const Influencers = props => {
  return (
    <Grid centered padded>
      <TopOccupation />

      <Grid.Row>
        <div style={{ margin: "0 auto", padding: "1rem" }}>
          <Card centered fluid>
            <Card.Content style={{ flex: "none" }}>
              <Card.Header textAlign="left">
                "ARTICLES & POST RESULTS"
              </Card.Header>
            </Card.Content>

            <Card.Content>
              <ResultTable />
            </Card.Content>
          </Card>
        </div>
      </Grid.Row>
    </Grid>
  );
};

export default Influencers;
