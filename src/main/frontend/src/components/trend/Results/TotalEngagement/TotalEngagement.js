import React from "react";
import PropTypes from "prop-types";
import { Card } from "semantic-ui-react";
import ReactEcharts from "echarts-for-react";

const TotalEngagement = props => {
  const getOption = () => ({
    tooltip: {
      trigger: "item",
      formatter: "{a} : {c}",
      axisPointer: {
        type: "cross",
        crossStyle: {
          color: "#999"
        }
      }
    },
    toolbox: {
      feature: {
        saveAsImage: {
          title: "Save As Image",
          pixelRatio: 10,
          emphasis: {
            iconStyle: {
              textAlign: "right"
            }
          }
        }
      }
    },
    legend: {
      data: props.legend, //
      bottom: 0
    },
    xAxis: [
      {
        type: "category",
        data: props.xAxis,
        axisPointer: {
          type: "shadow"
        }
      }
    ],
    yAxis: [
      {
        type: "value",
        name: "",
        min: 0,
        max: 100,
        interval: 20,
        axisLabel: {
          formatter: "{value}  %"
        }
      },
      {
        type: "value",
        name: "",
        min: 0,
        max: 2500,
        interval: 500,
        axisLabel: {
          formatter: "{value}"
        }
      }
    ],
    series: [
      {
        name: "Facebook",
        type: "bar",
        data: props.facebook,
        itemStyle: {
          normal: {
            color: "#6F7A7F"
          }
        }
      },
      {
        name: "Twitter",
        type: "bar",
        data: props.twitter,
        itemStyle: {
          normal: {
            color: "#98A0A3"
          }
        }
      },
      {
        name: "Instagram",
        type: "bar",
        data: props.instagram,
        itemStyle: {
          normal: {
            color: "#7FD5F7"
          }
        }
      },

      {
        name: "Youtube",
        type: "bar",
        data: props.youtube,
        itemStyle: {
          normal: {
            color: "#3DB2D2"
          }
        }
      },
      {
        name: "Total Engagemnet",
        type: "line",
        yAxisIndex: 1,
        data: props.total,
        markPoint: {
          data: [{ type: "max", name: "Max" }, { type: "min", name: "Min" }],
          symbolSize: 80,
          label: {
            formatter: "{c}",
            fontSize: "2rem"
          }
        },
        itemStyle: {
          normal: {
            color: "#175E71"
          }
        }
      }
    ]
  });

  return (
    <Card fluid>
      <Card.Content>
        <Card.Header textAlign="left">{props.title}</Card.Header>
      </Card.Content>

      <Card.Content>
        <ReactEcharts
          option={getOption()}
          style={props.styles}
          className={props.title}
        />
      </Card.Content>
    </Card>
  );
};

//set defult props

TotalEngagement.defaultProps = {
  title: "TotalEngagement",
  styles: { overflow: "hidden" },
  legend: ["Facebook", "Twitter", "Instagram", "Youtube", "Total Engagemnet"],
  xAxisData: [
    "01 Oct. 19",
    "02 Oct. 19",
    "03 Oct. 19",
    "04 Oct. 19",
    "05 Oct. 19"
  ],
  facebook: [20, 30, 10, 20, 5],
  twitter: [10, 20, 20, 30, 25],
  instagram: [30, 40, 20, 20, 30],
  youtube: [40, 10, 30, 30, 40],
  total: [400, 600, 1800, 2200, 2300]
};

TotalEngagement.propTypes = {
  title: PropTypes.string,
  styles: PropTypes.object,
  legend: PropTypes.array,
  xAxisData: PropTypes.array.isRequired,
  facebook: PropTypes.array,
  twitter: PropTypes.array,
  instagram: PropTypes.array,
  youtube: PropTypes.array,
  total: PropTypes.array
};

export default TotalEngagement;
