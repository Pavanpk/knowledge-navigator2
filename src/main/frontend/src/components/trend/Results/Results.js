import React from "react";
import ResultTable from "../../ui/ResultTable/ResultTable";
import { Grid, Card } from "semantic-ui-react";
import DoughnutChart from "../../ui/eCharts/Charts/DoughnutChart";
import LineChart from "../../ui/eCharts/Charts/LineChart";
import TotalEngagement from "./TotalEngagement/TotalEngagement";

const ShasreOfSource = () => {
  const _title = "Share of Source";
  const _styles = {};
  const _legend = ["Blogs", "Instagram", "Online News", "Forums", "Twitters"];
  const _data = [
    { name: "Blogs", value: 221 },
    { name: "Instagram", value: 147 },
    { name: "Online News", value: 48 },
    { name: "Forums", value: 25 },
    { name: "Twitters", value: 18 }
  ];

  return (
    <DoughnutChart
      title={_title}
      legend={_legend}
      data={_data}
      styles={_styles}
    />
  );
};

const ResultsOverTime = () => {
  const _title = "Results Over Time";
  const _xAxisData = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
  const _yAxisData = [820, 600, 901, 934, 1290, 1330, 1320];

  return (
    <LineChart title={_title} xAxisData={_xAxisData} yAxisData={_yAxisData} />
  );
};

const Results = props => {
  return (
    <Grid centered padded>
      <Grid.Row columns="2">
        <Grid.Column mobile={16} tablet={16} computer={6}>
          <ShasreOfSource />
        </Grid.Column>
        <Grid.Column mobile={16} tablet={16} computer={10}>
          <ResultsOverTime />
        </Grid.Column>
      </Grid.Row>

      <Grid.Row>
        <div style={{ width: "100%", padding: "1rem" }}>
          <TotalEngagement />
        </div>
      </Grid.Row>

      <Grid.Row>
        <div style={{ margin: "0 auto", padding: "1rem" }}>
          <Card fluid>
            <Card.Content style={{ flex: "none" }}>
              <Card.Header textAlign="left">
                "ARTICLE & POST RESULTS"
              </Card.Header>
            </Card.Content>

            <Card.Content>
              <ResultTable />
            </Card.Content>
          </Card>
        </div>
      </Grid.Row>
    </Grid>
  );
};

export default Results;
