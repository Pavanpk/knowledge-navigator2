import React from "react";
import StackedLineChart from "../../../ui/eCharts/Charts/StackedLineChart";

const EmotionOverTime = () => {
  const _title = "Emotion Over Time";
  const _legend = ["Disgust", "Anger", "Fear", "Joy", "Sadness", "Suprise"];
  const _xAxisData = [
    "01 Oct. 19",
    "02 Oct. 19",
    "03 Oct. 19",
    "04 Oct. 19",
    "05 Oct. 19",
    "06 Oct. 19",
    "07 Oct. 19"
  ];
  const _series = [
    {
      name: "Disgust",
      type: "bar",
      stack: "stack",
      data: [60, 100, 120, 64, 30, 50, 70],
      itemStyle: {
        normal: {
          color: "rgba(23, 94, 113,.1)"
        }
      }
    },
    {
      name: "Anger",
      type: "bar",
      stack: "stack",
      data: [120, 200, 250, 134, 90, 110, 130],
      itemStyle: {
        normal: {
          color: "rgba(23, 94, 113,.2)"
        }
      }
    },
    {
      name: "Fear",
      type: "bar",
      stack: "stack",
      data: [120, 200, 250, 134, 90, 110, 130],
      itemStyle: {
        normal: {
          color: "rgba(23, 94, 113,.4)"
        }
      }
    },
    {
      name: "Joy",
      type: "bar",
      stack: "stack",
      data: [220, 300, 350, 234, 110, 210, 530],
      itemStyle: {
        normal: {
          color: "rgba(23, 94, 113,.6)"
        }
      }
    },
    {
      name: "Sadness",
      type: "bar",
      stack: "stack",
      data: [120, 200, 250, 134, 90, 110, 130],
      itemStyle: {
        normal: {
          color: "rgba(23, 94, 113,.8)"
        }
      }
    },
    {
      name: "Suprise",
      type: "bar",
      stack: "stack",
      data: [220, 400, 150, 234, 190, 210, 230],
      itemStyle: {
        normal: {
          color: "rgba(23, 94, 113, 1)"
        }
      }
    }
  ];
  const _styles = {};

  return (
    <StackedLineChart
      title={_title}
      legend={_legend}
      styles={_styles}
      xAxisData={_xAxisData}
      series={_series}
    />
  );
};

export default EmotionOverTime;
