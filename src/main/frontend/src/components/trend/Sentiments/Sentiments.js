import React from "react";

import { Grid } from "semantic-ui-react";

import ShareOfSentiment from "./ShareOfSentiment/ShareOfSentiment";
import SentimentOverTime from "./SentimentOverTime/SentimentOverTime";
import ShareOfEmotions from "./ShareOfEmotions/ShareOfEmotions";
import EmotionOverTime from "./EmotionOverTime/EmotionOverTime";
import KeywordMap from "./KeywordMap/KeywordMap";

const Sentiments = () => {
  return (
    <Grid centered padded>
      <Grid.Row columns="2">
        <Grid.Column mobile={16} tablet={16} computer={6}>
          <ShareOfSentiment />
        </Grid.Column>
        <Grid.Column mobile={16} tablet={16} computer={10}>
          <SentimentOverTime />
        </Grid.Column>
      </Grid.Row>

      <Grid.Row columns="2">
        <Grid.Column mobile={16} tablet={16} computer={6}>
          <ShareOfEmotions />
        </Grid.Column>
        <Grid.Column mobile={16} tablet={16} computer={10}>
          <EmotionOverTime />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row columns="1">
        <Grid.Column width={16}>
          <KeywordMap />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default Sentiments;
