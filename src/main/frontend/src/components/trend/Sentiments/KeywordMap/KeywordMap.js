import React from "react";
import Tree from "../../../ui/eCharts/Charts/Tree";

const KeywordMap = () => {
  const title = "Tree gragh for Trend keyword";
  const tree_name = "Cannabis";
  const tree_data = [
    {
      name: "Cannabis",
      children: [
        {
          name: "inflammatory disorders",
          children: [
            {
              name: "stiffness",
              children: [
                { name: "swelling", value: 721 },
                { name: "stiffness", value: 4294 },
                { name: "rheumatology", value: 6000 },
                { name: "range of motion", value: 4294 },
                { name: "osteoarthritis", value: 6000 }
              ]
            },
            {
              name: "autoimmune disorder",
              value: 3322
            }
          ]
        },
        {
          name: "Short Attention Span",
          children: [
            { name: "joint", value: 8833 },
            { name: "wear and tear", value: 1732 },
            { name: "joint pain", value: 3623 },
            { name: "LineSprite", value: 1732 },
            { name: "Adaptive Functioning Assessment", value: 3623 }
          ]
        },
        {
          name: "AFA",
          children: [{ name: "Sensory Motor Evaluation", value: 4116 }]
        },
        {
          name: "Impulsivity",
          children: [
            { name: "Hyperactivity", value: 1616 },
            { name: "Sensory Motor Evaluation", value: 1027 },
            { name: "Arithmetic", value: 3891 },
            { name: "Average", value: 891 },
            { name: "BinaryExpression", value: 2893 },
            {
              name: "Observation",
              children: [
                { name: "Sensory Motor Evaluation", value: 593 },
                { name: "Hyperactivity", value: 330 },
                { name: "average", value: 287 },
                { name: "count", value: 277 },
                { name: "distinct", value: 292 }
              ]
            },
            { name: "Minimum", value: 843 },
            { name: "Aggression", value: 1554 },
            { name: "Follow Routines", value: 970 },
            { name: "Gets Upset", value: 13896 },
            { name: "Obsessive Interests", value: 1594 },
            { name: "StringUtil", value: 4130 }
          ]
        },
        {
          name: "scale",
          children: [
            { name: "IScaleMap", value: 2105 },
            { name: "LinearScale", value: 1316 },
            { name: "LogScale", value: 3151 },
            { name: "OrdinalScale", value: 3770 },
            { name: "QuantileScale", value: 2435 }
          ]
        }
      ]
    }
  ];
  return <Tree title={title} tree_name={tree_name} tree_data={tree_data} />;
};

export default KeywordMap;
