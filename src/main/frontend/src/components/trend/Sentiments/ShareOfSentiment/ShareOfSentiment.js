import React from "react";
import DoughnutChart from "../../../ui/eCharts/Charts/DoughnutChart";

const ShareOfSentiment = () => {
  const _title = "Share Of Sentiment";
  const _styles = {};
  const _legend = ["Positive", "Nagative"];
  const _data = [
    { name: "Positive", value: "11664" },
    { name: "Nagative", value: "3994" }
  ];

  return (
    <DoughnutChart
      title={_title}
      styles={_styles}
      legend={_legend}
      data={_data}
    />
  );
};

export default ShareOfSentiment;
