import React from "react";
import DoughnutChart from "../../../ui/eCharts/Charts/DoughnutChart";

const ShareOfEmotions = () => {
  const _title = "Share Of Emotions";
  const _styles = {};
  const _legend = ["Disgust", "Anger", "Fear", "Joy", "Sadness", "Suprise"];
  const _data = [
    { name: "Disgust", value: "330" },
    { name: "Anger", value: "201" },
    { name: "Fear", value: "197" },
    { name: "Joy", value: "186" },
    { name: "Sadness", value: "163" },
    { name: "Suprise", value: "49" }
  ];

  return (
    <DoughnutChart
      title={_title}
      styles={_styles}
      legend={_legend}
      data={_data}
    />
  );
};

export default ShareOfEmotions;
