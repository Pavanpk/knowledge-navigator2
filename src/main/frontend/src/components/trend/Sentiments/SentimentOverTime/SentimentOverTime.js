import React from "react";
import StackedLineChart from "../../../ui/eCharts/Charts/StackedLineChart";

const SentimentOverTime = () => {
  const _title = "Sentiment Over Time";
  const _legend = ["Positive", "Negative"];
  const _xAxisData = [
    "01 Oct. 19",
    "02 Oct. 19",
    "03 Oct. 19",
    "04 Oct. 19",
    "05 Oct. 19"
  ];
  const _series = [
    {
      name: "Positive",
      type: "line",
      //stack: "stack",
      data: [120, 200, 250, 134, 90],
      itemStyle: {
        normal: {
          color: "#175E71"
        }
      }
    },
    {
      name: "Negative",
      type: "line",
      //stack: "stack",
      data: [-10, -30, -40, -5, -50],
      itemStyle: {
        normal: {
          color: "#3DB2D2"
        }
      }
    }
  ];
  const _styles = {};

  return (
    <StackedLineChart
      title={_title}
      legend={_legend}
      styles={_styles}
      xAxisData={_xAxisData}
      series={_series}
    />
  );
};

export default SentimentOverTime;
