import React from "react";
// import Layout from "../layout/Layout";
import Results from "./Results/Results";
import Sentiments from "./Sentiments/Sentiments";
import Influencers from "./Influencers/Influencers";
import Demographics from "./Demographics/Demographics";
import Webanalytics from "../Webanalytics/WebAnalytics";

import AppHeader from "../layout/AppHeader/AppHeader";
import AppFooter from "../layout/AppFooter/AppFooter";
import { Container, Icon } from "semantic-ui-react";

import "./Trend.scss";

export const AppContainer = props => {
  return (
    <React.Fragment>
      <Container style={{ maxWidth: "1200px" }}>{props.children}</Container>
    </React.Fragment>
  );
};

export const Trend = () => {
  const [activePageIndex, setAtivePageIndex] = React.useState(0);
  const page = [
    "Webanalytics",
    "Results",
    "Sentiments",
    "Influencers",
    "Demographics"
  ];

  const navHandler = (e, index) => {
    e.preventDefault();
    setAtivePageIndex(index);
  };

  const navigation = page.map((item, index) => {
    return (
      <a
        href="/"
        key={index}
        className={`pageItem ${
          index === activePageIndex ? "active" : "inactive"
        }`}
        onClick={e => navHandler(e, index)}
      >
        {item}
      </a>
    );
  });

  const userpannel = (
    <React.Fragment>
      <Icon name="setting" />
      <Icon name="bell outline" />
      <a
        href="/"
        onClick={e => {
          e.preventDefault();
          console.log("you clicked usename");
        }}
      >
        Jongsun Park
      </a>

      <a
        href="/"
        onClick={e => {
          e.preventDefault();
          console.log("you clicked logout");
        }}
      >
        logout
      </a>
    </React.Fragment>
  );

  const activePage = activePageIndex => {
    switch (activePageIndex) {
      case 0:
        return <Webanalytics />;
      case 1:
        return <Results />;
      case 2:
        return <Sentiments />;
      case 3:
        return <Influencers />;
      case 4:
        return <Demographics />;
      default:
        return <Webanalytics />;
    }
  };

  return (
    <React.Fragment>
      <AppHeader />
      <div className="TopMenu">
        <div className="Navigation">{navigation}</div>
        <div className="UserPannel">{userpannel}</div>
      </div>

      <AppContainer>{activePage(activePageIndex)}</AppContainer>
      <AppFooter />
    </React.Fragment>
  );
};
