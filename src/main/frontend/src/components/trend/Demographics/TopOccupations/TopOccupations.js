import React from "react";
import HorizonBarChart from "../../../ui/eCharts/Charts/HorizonBarChart";

const TopOccupations = () => {
  const _title = "Top Occupations";
  const _yAxis = ["Scientist", "Entrepreneur"];
  const _series = [25, 75];

  return <HorizonBarChart title={_title} yAxis={_yAxis} series={_series} />;
};

export default TopOccupations;
