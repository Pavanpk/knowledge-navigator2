import React from "react";
import HorizonBarChart from "../../../ui/eCharts/Charts/HorizonBarChart";

const Age = () => {
  const _title = "Age";
  const _yAxis = ["65+", "55-64", "45-54", "34-44", "25-34", "18-24"];
  const _series = [4, 10, 14, 5, 150, 20];

  return <HorizonBarChart title={_title} yAxis={_yAxis} series={_series} />;
};

export default Age;
