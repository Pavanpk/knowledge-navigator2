import React from "react";
import ReactEcharts from "echarts-for-react";
import { Card } from "semantic-ui-react";
import "./PictorialBar.scss";

const PictorialBar = props => {
  const symbols = [
    "path://M1245.1,431.8v839.5c0,0-13.8-0.2-35.8-0.6v576.6c0,50.5-41.4,91.9-91.9,91.9c-50.6,0-91.9-41.4-91.9-91.9" +
      "v-579.6c-52.8-0.8-97.1-1.5-107.2-1.5c-28.6,0-36.8-22.5-19.5-60.3c17.4-37.8,155.3-519.8,155.3-535.1c0-15.2-20.3-14.6-26.3,2.5" +
      "L894,1063.3c-12.8,37.2-53.6,57.2-90.8,44.5h0c-37.2-12.8-57.2-53.6-44.4-90.8l140-408.5c2.8-7.6,5.5-15.7,8.3-24.2" +
      "c21.4-65.9,44.4-152.5,112.3-152.5C1096,431.8,1245.1,431.8,1245.1,431.8z M907.1,584.3c-2.8,8.5-5.5,16.6-8.3,24.2L907.1,584.3z " +
      "M1245.1,64.7v325.8c-90,0-162.9-72.9-162.9-162.9C1082.2,137.6,1155.1,64.7,1245.1,64.7z",
    "path://M1358.4,550.3V1114c0,39.3-32.2,71.5-71.5,71.5c-39.3,0-71.5-32.2-71.5-71.5V687.6c0-22-28.6-14.8-28.6-0.5" +
      "v545.4c0,24.4,0,38.8,0,38.8v576c0,50.5-41.4,91.9-91.9,91.9c-50.5,0-91.9-41.4-91.9-91.9v-576h-13.6V431.8c0,0,161.4,0,253.3,0" +
      "C1334.6,431.8,1358.4,517.6,1358.4,550.3z M1152.3,227.5c0,90-72.9,162.9-162.9,162.9V64.7C1079.4,64.7,1152.3,137.6,1152.3,227.5z"
  ];

  const bodyMax = 100;

  const labelSetting = {
    normal: {
      show: false,
      position: "bottom",
      offset: [0, 30],
      formatter: function(param) {
        param.name = param.name[0];
        return (
          param.name + " " + ((param.value / bodyMax) * 100).toFixed(1) + "%"
        );
      },
      textStyle: {
        fontSize: 20,
        fontFamily: "Lato",
        color: "#383838",
        fontWeight: "500"
      }
    }
  };

  const getOption = () => ({
    // barWidth: "95%",
    tooltip: {
      trigger: "axis",
      axisPointer: {
        type: "shadow"
      },
      formatter: "{b} {c} %"
    },
    toolbox: {
      feature: {
        saveAsImage: {
          title: "Save As Image",
          pixelRatio: 10,
          emphasis: {
            iconStyle: {
              textAlign: "right"
            }
          }
        }
      }
    },

    legend: {
      data: ["typeA"],
      selectedMode: "single",
      show: false
    },
    xAxis: {
      data: ["Female", "Male"],
      axisTick: { show: false },
      axisLine: { show: false },
      axisLabel: {
        show: false
      }
    },
    yAxis: {
      max: bodyMax,
      offset: 20,
      splitLine: { show: false },
      show: false
    },
    grid: {
      left: "35%",
      right: "35%",
      top: "10%",
      bottom: "10%"
      //   height: 140,
      //   width: 200
    },
    markLine: {
      z: -100
    },
    series: [
      {
        name: "typeA",
        type: "pictorialBar",
        symbolClip: true,
        symbolBoundingData: bodyMax,
        label: labelSetting,
        itemStyle: {
          normal: {
            color: ["#F28969"] //'#28AACC'
          }
        },
        data: [
          {
            value: props.options.female,
            symbol: symbols[0],
            itemStyle: {
              normal: {
                color: ["#F28969"]
              }
            },
            symbolSize: ["140%", "100%"]
          },
          {
            value: props.options.male,
            symbol: symbols[1],
            itemStyle: {
              normal: {
                color: ["#28AACC"]
              }
            }
          }
        ],
        z: 10
      },
      {
        z: 0,
        name: "full",
        type: "pictorialBar",
        symbolBoundingData: bodyMax,
        animationDuration: 0,
        itemStyle: {
          normal: {
            color: "#ccc"
            //borderColor: "lightgrey",
            //borderWidth: "2"
          }
        },
        data: [
          {
            value: 1,
            symbol: symbols[0],
            symbolSize: ["140%", "100%"]
          },
          {
            value: 1,
            symbol: symbols[1]
          }
        ]
      }
    ]
  });

  return (
    <div style={{ margin: "0 auto" }} className="PictorialBar">
      <Card centered fluid>
        <Card.Content style={{ flex: "none" }}>
          <Card.Header>{props.options.title}</Card.Header>
        </Card.Content>

        <Card.Content>
          <ReactEcharts
            option={getOption()}
            notMerge={true}
            lazyUpdate={true}
            className="ReportChart"
            style={{
              margin: "0rem auto",
              width: "80%",
              overflow: "none",
              height: "280px"
              //   padding: "1rem"
              //border: "1px solid black"
            }}
          />
        </Card.Content>
        <div className="Label">
          <span
            style={{
              float: "left",
              color: "#F28969"
            }}
          >
            {props.options.female} <small>%</small>
          </span>
          <span
            style={{
              float: "right",
              color: "#28AACC"
            }}
          >
            {props.options.male} <small>%</small>
          </span>
        </div>
      </Card>
    </div>
  );
};

export default PictorialBar;
