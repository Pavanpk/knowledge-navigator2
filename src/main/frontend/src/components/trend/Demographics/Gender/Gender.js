import React from "react";
import PictorialBar from "./PictorialBar/PictorialBar";

const Gender = () => {
  const options = {
    title: "Gender",
    female: "42.9",
    male: "57.1"
  };

  return <PictorialBar options={options} />;
};

export default Gender;
