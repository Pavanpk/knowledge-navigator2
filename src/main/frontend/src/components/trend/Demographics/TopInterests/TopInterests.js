import React from "react";
import Treemap from "../../../ui/eCharts/Charts/Treemap";

const TopInterests = () => {
  const _title = "Top Interests";
  const _topics = [
    "Global News",
    "Global Beauty",
    "Food & Drinks",
    "Fitness & Health",
    "Fashion",
    "Family and Parenting",
    "Celebrities & Entertainment",
    "Apparel",
    "Animals",
    "Advertising & Marketing"
  ];
  const _values = [160, 60, 40, 220, 40, 80, 180, 60, 60, 100];

  return <Treemap title={_title} topics={_topics} values={_values} />;
};

export default TopInterests;
