import React from "react";

import { Grid } from "semantic-ui-react";
import Gender from "./Gender/Gender";
import Age from "./Age/Age";
import TopInterests from "./TopInterests/TopInterests";
import TopOccupations from "./TopOccupations/TopOccupations";

const Demographics = props => {
  return (
    <Grid centered padded>
      <Grid.Row columns="2">
        <Grid.Column mobile={16} tablet={16} computer={6}>
          <Gender />
        </Grid.Column>
        <Grid.Column mobile={16} tablet={16} computer={10}>
          <Age />
        </Grid.Column>
      </Grid.Row>

      <Grid.Row columns="2">
        <Grid.Column mobile={16} tablet={16} computer={8}>
          <TopOccupations />
        </Grid.Column>
        <Grid.Column mobile={16} tablet={16} computer={8}>
          <TopInterests />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};
export default Demographics;
