import React from "react";
import ReactEcharts from "echarts-for-react";
// Aux from "../../../hoc/React.Fragmentilitary/React.Fragmentilitary";
import { Card } from "semantic-ui-react";

const getOption = () => ({
  dataset: {
    source: [
      ["Sesstions", "Product Checkouts", "Landing Page"],
      [4, 214, "bloombotanics.co.uk"],
      [3, 126, "/collections/1..."],
      [2, 126, "/collections/2..."],
      [2, 96, "/collections./3.."],
      [1, 74, "/products/1..."],
      [1, 50, "/products/2..."],
      [1, 33, "/pages/1..."],
      [1, 18, "/collections/4..."]
    ]
  },

  tooltip: {
    trigger: "axis",
    axisPointer: {
      type: "shadow"
    }
  },

  grid: { containLabel: true },

  xAxis: { name: "" },

  yAxis: { type: "category" },
  visualMap: {
    orient: "horizontal",
    left: "center",
    min: 1,
    max: 5,
    text: ["High Session", "Low Session"],
    // Map the score column to color
    dimension: 0,
    inRange: {
      color: ["#51BAD7", "#175E71"]
    }
  },
  series: [
    {
      type: "bar",
      encode: {
        // Map the "amount" column to X axis.
        x: "Product Checkouts",
        // Map the "product" column to Y axis
        y: "Landing Page"
      },
      label: {
        normal: {
          show: true,
          position: "insideRight"
        }
      },
      stack: "stacked"
    },
    {
      name: "Avg. Sesstions",
      type: "bar",
      stack: "stacked",
      label: {
        normal: {
          show: true,
          position: "right",
          formatter: `{c} s`
        }
      },
      color: "black",
      data: [4.006, 1.288, 1.228, 1.568, 1.301, 1.288, 1.568, 1]
    }
  ]
});

const LandingPage = () => {
  return (
    <React.Fragment>
      <Card fluid>
        <Card.Content style={{ flex: "none" }}>
          <Card.Header>Landing Page & Checkouts & Sessions</Card.Header>
        </Card.Content>

        <Card.Content>
          <ReactEcharts option={getOption()} />
        </Card.Content>
      </Card>
    </React.Fragment>
  );
};

export default LandingPage;
