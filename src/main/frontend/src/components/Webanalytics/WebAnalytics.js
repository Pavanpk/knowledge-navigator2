import React from "react";
import { Grid } from "semantic-ui-react";

import DeviceStat from "./DeviceStat/DeviceStat";
import ThreeCards from "./ThreeCards/ThreeCards";
import TotalSales from "./TotalSales/TotalSales";
import LandingPage from "./LandingPage/LandingPage";
import SourcePageView from "./SourcePageView/SourcePageView";
import CheckoutMap from "./CheckoutMap/CheckoutMap";

const WebAnalytics = props => {
  return (
    <Grid padded>
      <h2>Web Analytics Page</h2>
      <Grid.Row stretched className="GridRow">
        <Grid.Column mobile={16} tablet={16} computer={4}>
          <DeviceStat />
        </Grid.Column>
        <Grid.Column mobile={16} tablet={16} computer={12}>
          <ThreeCards />
        </Grid.Column>
      </Grid.Row>

      <Grid.Row stretched>
        <Grid.Column mobile={16} tablet={16} computer={8}>
          <TotalSales />
        </Grid.Column>
        <Grid.Column mobile={16} tablet={16} computer={8}>
          <LandingPage />
        </Grid.Column>
      </Grid.Row>

      <Grid.Row stretched>
        <Grid.Column mobile={16} tablet={16} computer={6}>
          <SourcePageView />
        </Grid.Column>
        <Grid.Column mobile={16} tablet={16} computer={10}>
          <CheckoutMap />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default WebAnalytics;
