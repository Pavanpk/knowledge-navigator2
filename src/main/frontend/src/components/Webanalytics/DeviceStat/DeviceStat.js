import React from "react";
import { Card, Grid } from "semantic-ui-react";

const device = [
  { name: "mobile", amount: 267, session: "01:22", share: "52" },
  { name: "desktop", amount: 183, session: "05:22", share: "36" },
  { name: "tablet", amount: 52, session: "02:24", share: "12" }
];

const deviceList = device.map(item => {
  const _styles = {
    fontSize: "1.4rem"
  };
  return (
    <Grid.Column key={item.name} stretched verticalAlign="middle">
      <p>{item.name}</p>
      <p>
        <span style={_styles}>{item.share}</span>%
      </p>
      amount: <p>{item.amount}</p>
      avg: {item.session}
    </Grid.Column>
  );
});

// margin / padding 0
// flex / flex-basis / background color

const colors = ["#3DB2D2", "#175E71", "#6F7A7E"];
const colorBar = colors.map((item, index) => {
  return (
    <div
      key={index}
      style={{ flexBasis: device[index].share + "%", backgroundColor: item }}
    >
      <span style={{ color: item }}>{device[index].share + "%"}</span>
    </div>
  );
});

const DeviceStat = () => {
  return (
    <Card fluid className="DeviceStat">
      <Card.Content style={{ flex: "none" }}>
        <Card.Header>Device Stat</Card.Header>
      </Card.Content>

      <Card.Content>
        <Grid style={{ margin: "0" }}>
          <Grid.Row columns={3}>{deviceList}</Grid.Row>
        </Grid>
      </Card.Content>

      <Card.Content
        extra
        style={{ display: "flex", margin: "0", padding: "0" }}
      >
        {colorBar}
      </Card.Content>
    </Card>
  );
};

export default DeviceStat;
