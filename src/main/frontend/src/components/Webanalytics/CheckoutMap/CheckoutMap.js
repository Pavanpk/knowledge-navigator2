import React from "react";
// Aux from "../../../hoc/React.Fragmentilitary/React.Fragmentilitary";
import { Card, Grid } from "semantic-ui-react";

import Map from "./Zoomable/Zoomable";
import CheckoutTable from "./CheckoutTable/CheckoutTable";

const CheckoutMap = () => {
  return (
    <React.Fragment>
      <Card fluid>
        <Card.Content>
          <Card.Header className="CardHeader">
            Checkout Map / Where they are
          </Card.Header>
        </Card.Content>
        <Card.Content>
          <Grid>
            <Grid.Column floated="left" width={8}>
              <Map />
            </Grid.Column>
            <Grid.Column floated="right" width={8}>
              <CheckoutTable />
            </Grid.Column>
          </Grid>
        </Card.Content>
      </Card>
    </React.Fragment>
  );
};

export default CheckoutMap;
