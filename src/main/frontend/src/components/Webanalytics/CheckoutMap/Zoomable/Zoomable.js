import React, { useEffect, useState } from "react";
import { csv } from "d3-fetch";
import { scaleLinear } from "d3-scale";
import {
  ComposableMap,
  Geographies,
  Geography,
  Sphere,
  Graticule,
  ZoomableGroup
} from "react-simple-maps";
import ReactTooltip from "react-tooltip";
// Aux from "../../../../hoc/React.Fragmentilitary/React.Fragmentilitary";

const geoUrl =
  "https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json";

const rounded = num => {
  if (num > 1000000000) {
    return Math.round(num / 100000000) / 10 + "Bn";
  } else if (num > 1000000) {
    return Math.round(num / 100000) / 10 + "M";
  } else {
    return Math.round(num / 100) / 10 + "K";
  }
};

const srcPath = `csv/vulnerability.csv`;

const colorScale = scaleLinear()
  .domain([0.29, 0.68])
  .range(["#E0E0E0", "#175E71"]);

const Map = ({ setTooltipContent }) => {
  const [data, setData] = useState([]);
  const [zoom, setZoom] = useState(1);

  useEffect(() => {
    csv(srcPath).then(_data => {
      setData(_data);
    });
  }, []);

  function handleZoomIn() {
    if (zoom >= 4) return;
    setZoom(zoom * 2);
  }

  function handleZoomOut() {
    if (zoom <= 1) return;
    setZoom(zoom / 2);
  }

  function handleZoomEnd(position) {
    setZoom(position.zoom);
  }

  return (
    <React.Fragment>
      <ComposableMap
        data-tip=""
        projectionConfig={{
          rotate: [-10, 0, 0],
          scale: 147
        }}
      >
        <ZoomableGroup zoom={zoom} onZoomEnd={handleZoomEnd}>
          <Sphere stroke="#fff" strokeWidth={0.5} />
          <Graticule stroke="#fff" strokeWidth={0.5} />
          {data.length > 0 && (
            <Geographies geography={geoUrl}>
              {({ geographies }) =>
                geographies.map(geo => {
                  const d = data.find(s => s.ISO3 === geo.properties.ISO_A3);
                  return (
                    <Geography
                      key={geo.rsmKey}
                      geography={geo}
                      onMouseEnter={() => {
                        const { NAME, POP_EST } = geo.properties;
                        setTooltipContent(`${NAME} — ${rounded(POP_EST)}`);
                      }}
                      onMouseLeave={() => {
                        setTooltipContent("");
                      }}
                      style={{
                        default: {
                          fill: d ? colorScale(d["2017"]) : "#D6D6DA",
                          outline: "none"
                        },
                        hover: {
                          fill: "#175E71",
                          outline: "none"
                        },
                        pressed: {
                          fill: "#6F7A7F",
                          outline: "none"
                        }
                      }}
                    />
                  );
                })
              }
            </Geographies>
          )}
        </ZoomableGroup>
      </ComposableMap>

      <div className="controls">
        <button onClick={handleZoomIn}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            stroke="currentColor"
            strokeWidth="3"
          >
            <line x1="12" y1="5" x2="12" y2="19" />
            <line x1="5" y1="12" x2="19" y2="12" />
          </svg>
        </button>
        <button onClick={handleZoomOut}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            stroke="currentColor"
            strokeWidth="3"
          >
            <line x1="5" y1="12" x2="19" y2="12" />
          </svg>
        </button>
      </div>
    </React.Fragment>
  );
};

const Zoomable = () => {
  const [content, setContent] = useState("");

  return (
    <React.Fragment>
      <Map setTooltipContent={setContent} />
      <ReactTooltip>{content}</ReactTooltip>
    </React.Fragment>
  );
};

export default Zoomable;
