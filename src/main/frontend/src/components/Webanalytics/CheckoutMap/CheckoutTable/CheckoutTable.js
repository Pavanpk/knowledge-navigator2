import _ from "lodash";
import React, { Component } from "react";
import { Table, Flag } from "semantic-ui-react";
// Aux from "../../../../hoc/React.Fragmentilitary/React.Fragmentilitary";

const tableData = [
  { name: "united kingdom", checkout: 835, avg: "37.53" },
  { name: "ireland", checkout: 32, avg: "54.00" },
  { name: "australia", checkout: 22, avg: "31.98" },
  { name: "france", checkout: 20, avg: "33.59" },
  { name: "hungary", checkout: 18, avg: "19.07" },
  { name: "thailand", checkout: 18, avg: "22.09" }
];

class CheckoutTable extends Component {
  state = {
    column: null,
    data: tableData,
    direction: null
  };

  handleSort = clickedColumn => () => {
    const { column, data, direction } = this.state;

    if (column !== clickedColumn) {
      this.setState({
        column: clickedColumn,
        data: _.sortBy(data, [clickedColumn]),
        direction: "ascending"
      });

      return;
    }

    this.setState({
      data: data.reverse(),
      direction: direction === "ascending" ? "descending" : "ascending"
    });
  };

  render() {
    const { column, data, direction } = this.state;

    return (
      <React.Fragment>
        <Table sortable celled fixed basic compact>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell
                sorted={column === "name" ? direction : null}
                onClick={this.handleSort("name")}
              >
                Country
              </Table.HeaderCell>
              <Table.HeaderCell
                sorted={column === "checkout" ? direction : null}
                onClick={this.handleSort("checkout")}
              >
                Order
              </Table.HeaderCell>
              <Table.HeaderCell
                sorted={column === "avg" ? direction : null}
                onClick={this.handleSort("avg")}
              >
                avg. Revenue
              </Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {_.map(data, ({ checkout, avg, name }) => (
              <Table.Row key={name}>
                <Table.Cell>
                  <Flag name={name}></Flag>
                  <span style={{ textTransform: "capitalize" }}>{name}</span>
                </Table.Cell>
                <Table.Cell>{checkout}</Table.Cell>
                <Table.Cell>{avg}</Table.Cell>
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
        <p style={{ textSize: ".8rem", opacity: ".8" }}>
          1 of {tableData.length} results
        </p>
      </React.Fragment>
    );
  }
}

export default CheckoutTable;
