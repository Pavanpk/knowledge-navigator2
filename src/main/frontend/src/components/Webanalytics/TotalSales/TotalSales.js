import React from "react";
// Aux from "../../../hoc/React.Fragmentilitary/React.Fragmentilitary";
import { Card, Grid } from "semantic-ui-react";
import ReactEcharts from "echarts-for-react";

const TotalSales = props => {
  const getOption = () => ({
    tooltip: {
      trigger: "axis",
      axisPointer: {
        type: "cross",
        crossStyle: {
          color: "#999"
        }
      }
    },

    toolbox: {
      feature: {
        saveAsImage: {
          title: "Save As Image",
          pixelRatio: 10,
          emphasis: {
            iconStyle: {
              textAlign: "right"
            }
          }
        }
      }
    },

    legend: {
      data: ["Quart 1", "Quart 2", "Quart 3", "Quart 4", "Total Sales"],
      top: "bottom"
    },

    xAxis: [
      {
        type: "category",
        data: ["date 1", "date 2", "date 3", "date 4", "date 5", "date 6"],
        axisPointer: {
          type: "shadow"
        }
      }
    ],

    yAxis: [
      {
        type: "value",
        name: "Quart Basis",
        min: 0,
        max: 1000,
        interval: 250,
        axisLabel: {
          formatter: "{value}"
        }
      },
      {
        type: "value",
        name: "Entire Time Basis",
        min: 0,
        max: 13000,
        interval: 13000,
        axisLabel: {
          formatter: "{value}"
        }
      }
    ],

    styles: {},

    series: [
      {
        name: "Quart 1",
        type: "bar",
        data: [400, 200, 100, 200, 900, 500],
        itemStyle: {
          normal: {
            color: "rgba(61, 178, 210, .4)"
          }
        },
        markPoint: {
          data: [{ name: "Hightest", value: 900, xAxis: 4, yAxis: 900 }]
        }
        // markPoint: {
        //   data: [{ type: "max", name: "Hightest" }, { type: "min", name: "Lowest" }]
        // }
        // sales value for 1 Quart for each time period
      },
      {
        name: "Quart 2",
        type: "bar",
        data: [700, 400, 500, 400, 200, 800],
        itemStyle: {
          normal: {
            color: "rgba(61, 178, 210, .6)"
          }
        }
      },
      {
        name: "Quart 3",
        type: "bar",
        data: [500, 400, 600, 200, 400, 500],
        itemStyle: {
          normal: {
            color: "rgba(61, 178, 210, .8)"
          }
        }
      },
      {
        name: "Quart 4",
        type: "bar",
        data: [800, 600, 200, 100, 700, 200],
        itemStyle: {
          normal: {
            color: "rgba(61, 178, 210, 1)"
          }
        },
        markPoint: {
          data: [{ name: "Lowest", value: 100, xAxis: 3, yAxis: 100 }]
        }
      },
      {
        name: "Total Sales",
        type: "line",
        yAxisIndex: 1,
        data: [2400, 4000, 5400, 6300, 8300, 11000],
        itemStyle: {
          normal: {
            color: "rgb(23, 94, 113)"
          }
        }
      }
    ]
  });

  return (
    <React.Fragment>
      <Grid.Column>
        <Card fluid>
          <Card.Content>
            <Card.Header>Total Sales</Card.Header>
          </Card.Content>
          <Card.Content>
            <div>
              <h3>23,212,53</h3>
              <p>01 Apr. 2019 ~ 31 Sep. 2019</p>
              <ReactEcharts
                option={getOption()}
                //style={props.styles}
                //className={props.title}
              />
            </div>
          </Card.Content>
        </Card>
      </Grid.Column>
    </React.Fragment>
  );
};

export default TotalSales;
