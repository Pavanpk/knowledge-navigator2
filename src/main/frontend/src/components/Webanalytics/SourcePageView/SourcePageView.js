import React from "react";
import ReactEcharts from "echarts-for-react";
// Aux from "../../../hoc/React.Fragmentilitary/React.Fragmentilitary";
import { Card } from "semantic-ui-react";

//import Dataset from "../../../../components/eCharts/DataSet/DataSet";
const getOption = () => ({
  tooltip: {
    trigger: "axis",
    axisPointer: {
      type: "shadow"
    }
  },
  legend: {
    data: ["Page View", "Users"],
    top: "bottom"
  },
  grid: {
    top: "10%",
    left: "5%",
    right: "5%",
    bottom: "15%",
    containLabel: true
  },
  xAxis: {
    type: "value",
    boundaryGap: [0, 0.01]
  },
  yAxis: {
    type: "category",
    data: [
      "google",
      "direct",
      "vivapayment",
      "herbalizestore",
      "bing",
      "facebook",
      "another",
      "othoers"
    ]
  },
  series: [
    {
      name: "Page View",
      type: "bar",
      data: [26045, 3890, 505, 459, 237, 204, 100, 50],
      label: {
        normal: {
          show: true,
          position: "right",
          formatter: `{c} `
        }
      },
      color: "#3DB2D2"
    },
    {
      name: "Users",
      type: "bar",
      data: [14510, 1568, 128, 2, 115, 126, 50, 25],
      label: {
        normal: {
          show: true,
          position: "right",
          formatter: `{c} `
        }
      },
      color: "#175E71"
    }
  ]
});

const SourcePageView = () => {
  return (
    <React.Fragment>
      <Card fluid>
        <Card.Content>
          <Card.Header>Source with Unique Pageviews and Users</Card.Header>
        </Card.Content>

        <Card.Content>
          <ReactEcharts option={getOption()} />
        </Card.Content>
      </Card>
    </React.Fragment>
  );
};

export default SourcePageView;
