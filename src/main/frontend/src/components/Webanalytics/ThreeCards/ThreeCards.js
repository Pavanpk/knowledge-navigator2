import React from "react";
// Aux from "../../../hoc/React.Fragmentilitary/React.Fragmentilitary";
import { Card, Grid, Icon } from "semantic-ui-react";

import MiniChart from "./MiniChart/MiniChart";

import "./ThreeCards.scss";

const miniChartData = [
  {
    name: "Total Visitors",
    sub: "21,230",
    detail: [3000, 2700, 2900, 3100, 3500, 3800]
  },
  {
    name: "Total Transaction",
    sub: "502",
    detail: [90, 110, 130, 80, 120, 150]
  },
  {
    name: "Avg Order Value",
    sub: "£ 45.21",
    detail: [20, 30, 25, 33, 45, 50]
  }
];

const _date = ["Oct 9", "Oct 10", "Oct 11", "Oct 12", "Oct 13", "Oct 14"];

const miniChartDataList = miniChartData.map((item, index) => {
  return (
    <Grid.Column key={item.name}>
      <Card fluid className="Borderless">
        <Card.Content>
          <Card.Header className="CardHeader">{item.name}</Card.Header>
        </Card.Content>
        <Card.Content>
          <span className="ItemSub left">{item.sub}</span>
          <span className="right red">
            5.2 %<Icon name="arrow up" size="small" />
          </span>

          <MiniChart dataX={_date} dataY={item.detail} />
        </Card.Content>
      </Card>
    </Grid.Column>
  );
});

const ThreeCards = () => {
  return (
    <React.Fragment>
      <Grid
        columns="equal"
        celled
        className="CardRow"
        style={{ margin: 0, borderRadius: "4px" }}
      >
        <Grid.Row>{miniChartDataList}</Grid.Row>
      </Grid>
    </React.Fragment>
  );
};

export default ThreeCards;
