import React from "react";
import ReactEcharts from "echarts-for-react";
import "./MiniChart.scss";

const MiniChart = props => {
  const getOption = () => ({
    tooltip: {
      trigger: "axis",
      axisPointer: {
        type: "shadow"
      }
    },
    grid: {
      top: "20%",
      left: "0%",
      right: "15%",
      bottom: "0%",
      containLabel: true
    },
    xAxis: [
      {
        type: "category",
        data: props.dataX,
        axisTick: {
          alignWithLabel: true
        },
        show: false
      }
    ],
    yAxis: [
      {
        type: "value",
        show: false
      }
    ],
    series: [
      {
        name: "",
        type: "bar",
        barWidth: "70%",

        itemStyle: {
          normal: {
            color: "rgb(61, 178, 210)"
          },
          emphasis: {
            color: "rgb(23, 94, 113)"
          }
        },
        data: props.dataY
      }
    ]
  });

  return <ReactEcharts className="MiniChart" option={getOption()} />;
};

export default MiniChart;
