import React from "react";
import PropTypes from "prop-types";
import { Card, Image, Grid, Button } from "semantic-ui-react";
import * as faker from "faker";

const randomNumberGenerator = () => {
  const numberWithCommas = x => {
    x = x.toString();
    var pattern = /(-?\d+)(\d{3})/;
    while (pattern.test(x)) x = x.replace(pattern, "$1,$2");
    return x;
  };

  return numberWithCommas(faker.fake("{{random.number}}"));
};

const CardBox = props => {
  return (
    <div style={{ width: "fit-content", margin: "0 auto" }}>
      <Card centered fluid>
        <Card.Content style={{ flex: "none" }}>
          <Card.Header>{props.title}</Card.Header>
        </Card.Content>

        <Grid style={{ padding: "1rem" }} columns={2} /* relaxed="very" */>
          <Grid.Column>
            <Card.Header>
              <h3 className="CardH">
                {faker.fake("{{name.lastName}}, {{name.firstName}}")}
              </h3>
              <Button basic compact size="mini">
                Read more
              </Button>
            </Card.Header>
            <Card.Meta></Card.Meta>
          </Grid.Column>
          <Grid.Column verticalAlign="middle">
            <Image
              src={faker.fake("{{internet.avatar}}")}
              size="tiny"
              circular
              spaced
            />
          </Grid.Column>
        </Grid>

        <Card.Content>
          <Grid columns={2} relaxed="very">
            <Grid.Column>Engagements</Grid.Column>
            <Grid.Column>
              <span style={{ fontSize: "1rem", fontWeight: "bold" }}>
                {randomNumberGenerator()}
              </span>
            </Grid.Column>
          </Grid>
        </Card.Content>

        {/* <Card.Content>{props.children}</Card.Content> */}
      </Card>
    </div>
  );
};

CardBox.defaultProps = {
  title: "Title"
};

CardBox.propTypes = {
  title: PropTypes.string
};

export default CardBox;
