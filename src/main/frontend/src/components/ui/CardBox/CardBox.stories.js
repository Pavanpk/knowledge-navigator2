import React from "react";
import { storiesOf } from "@storybook/react";
import { Grid } from "semantic-ui-react";

import CardBox from "./CardBox";

storiesOf("Components | Card Box ", module)
  .addParameters({
    info: { inline: true }
  })

  .add(
    "Default",
    () => {
      const _title = "Most Active Author";

      return <CardBox title={_title}>//</CardBox>;
    },
    {
      info: {
        text: `
      ## Card Box
      Semantic UI Card Holder
      ~~~js
      <CardBox title={_title}>
        //
      </CardBox>
      ~~~
    `
      }
    }
  )

  .add(
    "4 Columns",
    () => {
      const _title = [
        "Most Active Author",
        "Most Influential Author",
        "Most Active Company",
        "Most Influential Company"
      ];

      const cardList = _title.map(item => {
        return (
          <Grid.Column>
            <CardBox title={item}></CardBox>
          </Grid.Column>
        );
      });

      return (
        <div>
          <Grid columns={4} padded stretched>
            {cardList}
          </Grid>
        </div>
      );
    },
    {
      info: {
        text: `
      ## Card Box
      Semantic UI Card Holder
      ~~~js
      <div>
        <Grid columns={4} padded stretched>
        {cardList}
        </Grid>
    </div>
      ~~~
    `
      }
    }
  );
