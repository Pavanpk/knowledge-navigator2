import React, { PureComponent } from "react";
import { CarouselProvider, Slider, Slide, DotGroup } from "pure-react-carousel";
import "pure-react-carousel/dist/react-carousel.es.css";
// import "../../App.scss";
import "./Carousel.scss";

export default class Carousel extends PureComponent {
  render() {
    return (
      <CarouselProvider
        className="CarouselProvider"
        naturalSlideWidth={100}
        naturalSlideHeight={125}
        totalSlides={2}
      >
        <Slider>
          <Slide className="Slide" index={0}>
            <h2>{this.props.title}</h2>
            <p>{this.props.sub}</p>
            <p>{this.props.desc}</p>
          </Slide>
          <Slide className="Slide" index={1}>
            <h2>{this.props.title}</h2>
            <p>{this.props.sub}</p>
            <p>{this.props.desc}</p>
          </Slide>
        </Slider>

        <div className="dotGroup">
          <DotGroup />
        </div>
      </CarouselProvider>
    );
  }
}
