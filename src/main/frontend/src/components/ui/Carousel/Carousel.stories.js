import React from "react";
import { storiesOf } from "@storybook/react";
import Carousel from "./Carousel";

storiesOf("Components", module)
  //   .addDecorator(withKnobs)
  .addParameters({
    info: { inline: true }
  })

  .add(
    "Carousel",
    () => {
      const _title = "What is the knowledge Navigator?";
      const _desc =
        "Empower your strategies with data & AI The Knowledge Navigator gives analytics picked up from Million’s of URLS using industry leading online & social listening techniques that can be filtered to niche demographics to suit any campaign. The Knowledge Navigator will hold your hand tracking your brand, how your mentioned and what you should say and to whom so you can safely profit from this generations largest cultural change.";

      return (
        <div style={{ width: "60vw" }}>
          <Carousel title={_title} desc={_desc} clssName="Carousel" />
        </div>
      );
    },
    {
      info: {
        text: `
      ## Carousel for login cover side
      ~~~js
      const _title = "What is the knowledge Navigator?";
      const _desc =
        "Empower your strategies with data & AI The Knowledge Navigator gives analytics picked up from Million’s of URLS using industry leading online & social listening techniques that can be filtered to niche demographics to suit any campaign. The Knowledge Navigator will hold your hand tracking your brand, how your mentioned and what you should say and to whom so you can safely profit from this generations largest cultural change.";

      return <Carousel title={_title} desc={_desc} />
      ~~~
    `
      }
    }
  );
