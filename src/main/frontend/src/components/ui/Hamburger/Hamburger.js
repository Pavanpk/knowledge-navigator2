import React from "react";
import "./Hamburger.scss";

const Hamburger = props => {
  return (
    <div
      id="nav-icon3"
      className={props.visible ? "open" : null}
      onClick={props.click}
    >
      <span></span>
      <span></span>
      <span></span>
      <span></span>
    </div>
  );
};

export default Hamburger;
