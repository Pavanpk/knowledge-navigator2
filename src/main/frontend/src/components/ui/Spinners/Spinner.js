// Single Element CSS Spinners
// https://projects.lukehaas.me/css-loaders/

import React from "react";
import "./Spinner.scss";

const Spinner = () => {
  return <div className="Loader">Loading...</div>;
  // Text will show when css doesn't work
};

export default Spinner;
