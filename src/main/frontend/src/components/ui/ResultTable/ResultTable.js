import React, { Component } from "react";
import * as faker from "faker";
import _ from "lodash";
import { Table, Pagination, Grid, Icon } from "semantic-ui-react";

// <Icon color='red' name='users' />
// cosnt l = source.length
// source[Math.floor(Math.random() * l + 1)]
// <Icon color='red' name={source[Math.floor(Math.random() * l + 1)]} />

const TableGenerater = () => {
  const table = [];
  let i = 0;
  const tableSize = 25;

  const source = ["twitter", "google", "mail", "facebook", "youtube"];
  const colors = ["teal", "black", "grey", "blue", "red"];
  const l = source.length;

  while (i < tableSize) {
    const _index = Math.floor(Math.random() * l);

    table[i] = {
      Id: i + 1,
      Source: {
        name: source[_index],
        color: colors[_index]
      },
      Title: faker.fake("{{lorem.sentence}}"),
      Description: faker.fake("{{lorem.sentence}}"),
      Date: faker.fake("{{date.month}}"),
      Sentimental: faker.fake("{{random.number}}"),
      Comment: faker.fake("{{random.number}}"),
      Shares: faker.fake("{{random.number}}")
    };
    i++;
  }
  return table;
};

const tableData = TableGenerater();

const PaginationSimple = () => (
  <Pagination defaultActivePage={1} totalPages={10} />
);

class ResultTable extends Component {
  state = {
    column: null,
    data: tableData,
    direction: null
  };

  handleSort = clickedColumn => () => {
    const { column, data, direction } = this.state;

    if (column !== clickedColumn) {
      this.setState({
        column: clickedColumn,
        data: _.sortBy(data, [clickedColumn]),
        direction: "ascending"
      });

      return;
    }

    this.setState({
      data: data.reverse(),
      direction: direction === "ascending" ? "descending" : "ascending"
    });
  };

  render() {
    const { column, data, direction } = this.state;

    return (
      <div>
        <Table sortable celled striped compact size="large">
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell
                sorted={column === "Source" ? direction : null}
                onClick={this.handleSort("Source.name")}
              >
                {/* ID */}
                SOURCE
              </Table.HeaderCell>
              <Table.HeaderCell
                sorted={column === "Title" ? direction : null}
                onClick={this.handleSort("Title")}
              >
                TITLE
              </Table.HeaderCell>
              <Table.HeaderCell
                sorted={column === "Description" ? direction : null}
                onClick={this.handleSort("Description")}
              >
                DESCRIPTION
              </Table.HeaderCell>
              <Table.HeaderCell
                sorted={column === "Date" ? direction : null}
                onClick={this.handleSort("Date")}
              >
                DATE
              </Table.HeaderCell>
              <Table.HeaderCell
                sorted={column === "Sentimental" ? direction : null}
                onClick={this.handleSort("Sentimental")}
              >
                SENTIMENTAL
              </Table.HeaderCell>
              <Table.HeaderCell
                sorted={column === "Comment" ? direction : null}
                onClick={this.handleSort("Comment")}
              >
                COMMENT
              </Table.HeaderCell>
              <Table.HeaderCell
                sorted={column === "Shares" ? direction : null}
                onClick={this.handleSort("Shares")}
              >
                SHARES
              </Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {_.map(
              data,
              ({
                Id,
                Source,
                Title,
                Description,
                Date,
                Sentimental,
                Comment,
                Shares
              }) => (
                <Table.Row key={Id} className="HoverCell">
                  <Table.Cell>
                    <Icon
                      color={Source.color}
                      name={Source.name}
                      size="large"
                    />
                  </Table.Cell>
                  <Table.Cell className="CapLetter">{Title}</Table.Cell>
                  <Table.Cell>{Description}</Table.Cell>
                  <Table.Cell>{Date}</Table.Cell>
                  <Table.Cell>{Sentimental}</Table.Cell>
                  <Table.Cell>{Comment}</Table.Cell>
                  <Table.Cell>{Shares}</Table.Cell>
                </Table.Row>
              )
            )}
          </Table.Body>
        </Table>
        <Grid centered columns={2}>
          <Grid.Column>
            <PaginationSimple />
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

export default ResultTable; //exporting a component make it reusable and this is the beauty of react
