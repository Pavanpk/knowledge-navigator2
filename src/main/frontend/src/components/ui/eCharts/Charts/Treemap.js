import React from "react";
import PropTypes from "prop-types";
import { Card } from "semantic-ui-react";
import ReactEcharts from "echarts-for-react";

const TreeMap = props => {
  const topics = props.topics;
  const values = props.values;

  const topiclist = topics.map((item, index) => {
    return { name: item, value: values[index] };
  });

  const getOption = () => ({
    tooltip: {
      formatter: `{b} <br> {c}`
    },
    series: [
      {
        name: props.title,
        type: "treemap",
        data: topiclist,
        levels: [
          {
            itemStyle: {
              normal: {
                borderColor: "#555",
                borderWidth: 4,
                gapWidth: 4
              }
            }
          },
          {
            colorSaturation: [0.3, 0.6],
            itemStyle: {
              normal: {
                borderColorSaturation: 0.7,
                gapWidth: 2,
                borderWidth: 2
              }
            }
          },
          {
            colorSaturation: [0.3, 0.5],
            itemStyle: {
              normal: {
                borderColorSaturation: 0.6,
                gapWidth: 1
              }
            }
          },
          {
            colorSaturation: [0.3, 0.5]
          }
        ]
      }
    ],
    animationDelay: 1000,
    animationDuration: 2000
  });

  return (
    <div style={{ margin: "0 auto" }}>
      <Card centered fluid>
        <Card.Content style={{ flex: "none" }}>
          <Card.Header>{props.title}</Card.Header>
        </Card.Content>

        <Card.Content>
          <ReactEcharts
            option={getOption()}
            style={props.styles}
            className="ReactEcharts"
          />
        </Card.Content>
      </Card>
    </div>
  );
};

//set defult props

TreeMap.defaultProps = {
  title: "Line Chart",
  topics: [
    "Global News",
    "Global Beauty",
    "Food & Drinks",
    "Fitness & Health",
    "Fashion",
    "Family and Parenting",
    "Celebrities & Entertainment",
    "Apparel",
    "Animals",
    "Advertising & Marketing"
  ],
  values: [160, 60, 40, 220, 40, 80, 180, 60, 60, 100]
};

TreeMap.propTypes = {
  title: PropTypes.string,
  topics: PropTypes.array.isRequired,
  values: PropTypes.array.isRequired
};

export default TreeMap;
