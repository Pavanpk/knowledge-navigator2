import React from "react";
import PropTypes from "prop-types";
import { Card } from "semantic-ui-react";
import ReactEcharts from "echarts-for-react";

// get data from parents component
// <StackedLineChart data={_data}/>
// StackedLineChart = () => ( data = {props.data} )

const StackedLineChart = props => {
  const getOption = () => ({
    tooltip: {
      trigger: "axis",
      axisPointer: {
        type: "shadow",
        label: {
          backgroundColor: "#6a7985"
        }
      }
    },

    legend: {
      data: props.legend,
      top: "bottom"
    },

    toolbox: {
      feature: {
        saveAsImage: {
          title: "Save As Image",
          pixelRatio: 10,
          emphasis: {
            iconStyle: {
              textAlign: "right"
            }
          }
        }
      }
    },

    grid: {
      left: "10%",
      right: "10%",
      bottom: "10%",
      containLabel: true
    },

    xAxis: [
      {
        type: "category",
        boundaryGap: true,
        data: props.xAxisData
      }
    ],
    yAxis: [
      {
        type: "value"
      }
    ],

    series: props.series //
  });

  return (
    <div>
      <Card centered fluid>
        <Card.Content style={{ flex: "none" }}>
          <Card.Header>{props.title}</Card.Header>
        </Card.Content>

        <Card.Content>
          <ReactEcharts
            option={getOption()}
            style={props.styles}
            className="ReactEcharts"
            // opts={{ renderer: "svg" }}
          />
        </Card.Content>
      </Card>
    </div>
  );
};

//set defult props

StackedLineChart.defaultProps = {
  title: "Stacked Area Chart",
  legend: ["data 1", "data 2", "data 3", "data 4", "data 5"],
  xAxisData: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
  // styles: {
  //   height: "300px",
  //   width: "500px",
  //   margin: "0 auto"
  // },

  series: [
    {
      name: "data 1",
      type: "line",
      stack: "stack",
      data: [120, 132, 101, 134, 90, 230, 210],
      itemStyle: { color: "#175E71" }
    },
    {
      name: "data 2",
      type: "line",
      stack: "stack",
      data: [220, 182, 191, 234, 290, 330, 310],
      itemStyle: { color: "#3DB2D2" }
    },
    {
      name: "data 3",
      type: "line",
      stack: "stack",

      data: [150, 232, 201, 154, 190, 330, 410],
      itemStyle: { color: "#98A0A3" }
    },
    {
      name: "data 4",
      type: "line",
      stack: "stack",

      data: [320, 332, 301, 334, 390, 330, 320],
      itemStyle: { color: "#6F7A7F" }
    },
    {
      name: "data 5",
      type: "line",
      stack: "stack",
      label: {
        normal: {
          show: true,
          position: "top"
        }
      },

      data: [820, 932, 901, 934, 1290, 1330, 1320],
      itemStyle: { color: "#46545A" },
      animationDelay: 1000,
      animationDuration: 2000
    }
  ]
};

StackedLineChart.propTypes = {
  title: PropTypes.string,
  legend: PropTypes.array,
  styles: PropTypes.object,
  xAxisData: PropTypes.array.isRequired,
  series: PropTypes.array.isRequired
};

export default StackedLineChart;
