import React from "react";
import PropTypes from "prop-types";
import { Card } from "semantic-ui-react";
import ReactEcharts from "echarts-for-react";

const DataSetChart = props => {
  const getOption = () => ({
    legend: {
      top: "bottom"
    },
    tooltip: {},
    dataset: {
      dimensions: props.dementions, // default
      source: props.source // default
    },
    toolbox: {
      feature: {
        saveAsImage: {
          title: "Save As Image",
          pixelRatio: 10,
          emphasis: {
            iconStyle: {
              textAlign: "right"
            }
          }
        }
      }
    },
    xAxis: { type: "category" },
    yAxis: {},
    // Declare several bar series, each will be mapped
    // to a column of dataset.source by default.
    series: [{ type: "bar" }, { type: "bar" }, { type: "bar" }],
    animationDelay: 1000,
    animationDuration: 2000
  });

  return (
    <div style={{ width: "fit-content", margin: "0 auto" }}>
      <Card centered fluid>
        <Card.Content style={{ flex: "none" }}>
          <Card.Header>{props.title}</Card.Header>
        </Card.Content>

        <Card.Content>
          <ReactEcharts
            option={getOption()}
            style={props.styles}
            className="ReactEcharts"
            //opts={{ renderer: "svg" }}
          />
        </Card.Content>
      </Card>
    </div>
  );
};

DataSetChart.defaultProps = {
  title: "Data Set Chart",
  dementions: ["product", "2015", "2016", "2017"],
  source: [
    ["Matcha Latte", 43.3, 85.8, 93.7],
    ["Milk Tea", 83.1, 73.4, 55.1],
    ["Cheese Cocoa", 86.4, 65.2, 82.5],
    ["Walnut Brownie", 72.4, 53.9, 39.1]
  ],
  styles: {
    height: "300px",
    width: "500px",
    margin: "0 auto"
  }
};

DataSetChart.propTypes = {
  title: PropTypes.string,
  styles: PropTypes.object,
  dementions: PropTypes.array.isRequired,
  source: PropTypes.array.isRequired
};

export default DataSetChart;
