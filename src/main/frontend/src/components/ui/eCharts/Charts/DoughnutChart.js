import React from "react";
import PropTypes from "prop-types";
import { Card } from "semantic-ui-react";
import ReactEcharts from "echarts-for-react";

const DoughnutChart = props => {
  const getOption = () => ({
    color: ["#175E71", "#3DB2D2", "#7FD5F7", "#98A0A3", "#6F7A7F", "#46545A"],
    tooltip: {
      trigger: "item",
      formatter: "{b}<br/>{c} ({d}%)"
      // "{b}<br/>{c} ({d}%)",
      //"{a} <br/>{b}: {c} ({d}%)"
      //style: { textAlign: "center" }
    },
    // visualMap: {
    //   show: false,
    //   inRange: {
    //     colorLightness: [0, 0.5]
    //   }
    // },
    legend: {
      bottom: 0,
      left: "center",
      data: props.legend // default
    },
    toolbox: {
      feature: {
        saveAsImage: {
          title: "Save As Image",
          pixelRatio: 10,
          emphasis: {
            iconStyle: {
              textAlign: "right"
            }
          }
        }
      }
    },
    series: [
      {
        name: props.title, //{a}
        type: "pie",
        radius: ["40%", "50%"],
        center: ["50%", "40%"],
        avoidLabelOverlap: false,
        label: {
          normal: {
            show: false,
            position: "center",
            textStyle: {
              fontSize: "18",
              fontWeight: "bold"
            },
            formatter: p => {
              return p.name + "\n" + p.value;
            }
          },
          emphasis: {
            show: true,
            textStyle: {
              fontSize: "18",
              fontWeight: "bold"
            },
            animationDuration: 1500
          },
          labelLine: {
            normal: {
              show: false
            }
          }
        },
        data: props.data, //dafualt,
        itemStyle: {
          // normal: {
          //   color: "#175E71"
          // },
          emphasis: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: "rgba(0, 0, 0, 0.5)"
          }
        }
      }
    ],
    animationDelay: 1000,
    animationDuration: 2000
  });

  return (
    <div style={{ margin: "0 auto" }}>
      <Card centered fluid>
        <Card.Content style={{ flex: "none" }}>
          <Card.Header>{props.title}</Card.Header>
        </Card.Content>
        <Card.Content>
          <ReactEcharts
            option={getOption()}
            style={props.styles}
            className="ReactEcharts"
            // opts={{ renderer: "svg" }}
          />
        </Card.Content>
      </Card>
    </div>
  );
};

DoughnutChart.defaultProps = {
  title: "Area Chart Title",
  styles: {
    height: "500px",
    width: "500px",
    margin: "0 auto"
  },
  legend: ["data1", "data2", "data3", "data4", "data5"],
  data: [
    { value: 1548, name: "data1" },
    { value: 535, name: "data2" },
    { value: 510, name: "data3" },
    { value: 634, name: "data4" },
    { value: 735, name: "data5" }
  ]
};

DoughnutChart.propTypes = {
  title: PropTypes.string,
  styles: PropTypes.object,
  legend: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired
};

export default DoughnutChart;
