import React from "react";
import PropTypes from "prop-types";
import { Card } from "semantic-ui-react";
import ReactEcharts from "echarts-for-react";

// get data from parents component
// <BarChart data={_data}/>
// Barchart = () => ( data = {props.data} )

const BarChart = props => {
  //const [title, styles, xAxisData, yAxisData] = props;

  const getOption = () => ({
    xAxis: {
      type: "category",
      boundaryGap: true, // area chart only // ture -> empty at the end of each side
      data: props.xAxisData
    },
    tooltip: {
      trigger: "axis",
      axisPointer: {
        type: "shadow"
      }
    },
    toolbox: {
      feature: {
        saveAsImage: {
          title: "Save As Image",
          pixelRatio: 10,
          emphasis: {
            iconStyle: {
              textAlign: "right"
            }
          }
        }
      }
    },

    yAxis: {
      type: "value"
    },

    series: [
      {
        data: props.yAxisData,
        type: "bar",
        areaStyle: {}, // area chart only,
        barMaxWidth: "20px",
        itemStyle: { color: "#175E71" }
      }
    ],
    animationDelay: 1000,
    animationDuration: 2000
  });

  return (
    <div style={{ width: "fit-content", margin: "0 auto" }}>
      <Card centered fluid>
        <Card.Content style={{ flex: "none" }}>
          <Card.Header>{props.title}</Card.Header>
        </Card.Content>

        <Card.Content>
          <ReactEcharts
            option={getOption()}
            style={props.styles}
            className="ReactEcharts"
            // opts={{ renderer: "svg" }}
          />
        </Card.Content>
      </Card>
    </div>
  );
};

//set defult props

BarChart.defaultProps = {
  title: "Bar Chart Title",
  styles: {
    height: "300px",
    width: "500px",
    margin: "0 auto"
  },
  xAxisData: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
  yAxisData: [820, 600, 901, 934, 1290, 1330, 1320]
};

BarChart.propTypes = {
  title: PropTypes.string,
  styles: PropTypes.object,
  xAxisData: PropTypes.array.isRequired,
  yAxisData: PropTypes.array.isRequired
};

export default BarChart;
