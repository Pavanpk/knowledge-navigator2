import React from "react";
import PropTypes from "prop-types";
import { Card } from "semantic-ui-react";
import ReactEcharts from "echarts-for-react";

const PieChart = props => {
  const getOption = () => ({
    color: ["#175E71", "#3DB2D2", "#7FD5F7", "#98A0A3", "#6F7A7F", "#46545A"],
    tooltip: {
      trigger: "item",
      formatter: "{b}<br/>{c} ({d}%)",
      style: { textAlign: "center" }
    },
    toolbox: {
      feature: {
        saveAsImage: {
          title: "Save As Image",
          pixelRatio: 10,
          emphasis: {
            iconStyle: {
              textAlign: "right"
            }
          }
        }
      }
    },

    legend: {
      bottom: 0,
      left: "center",
      data: props.legend // default
    },
    series: [
      {
        name: "Basic PieChart", //{a}
        type: "pie",
        radius: "50%",
        center: ["50%", "50%"],
        selectedMode: "single",

        label: {
          normal: {
            show: true,
            position: "outside",
            formatter: "{b} \n {c} ({d}%)",
            textStyle: {
              fontSize: "15"
            }
          },
          emphasis: {
            show: true,
            textStyle: {
              fontSize: "15",
              fontWeight: "bold"
            }
          },
          labelLine: {
            normal: {
              show: false
            }
          }
        },
        data: props.data, //dafualt,
        itemStyle: {
          emphasis: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: "rgba(0, 0, 0, 0.5)"
          }
        }
      }
    ],
    animationDelay: 1000,
    animationDuration: 2000
  });

  return (
    <div style={{ width: "fit-content", margin: "0 auto" }}>
      <Card centered fluid>
        <Card.Content style={{ flex: "none" }}>
          <Card.Header>{props.title}</Card.Header>
        </Card.Content>
        <Card.Content>
          <ReactEcharts
            option={getOption()}
            style={props.styles}
            className="ReactEcharts"
            // opts={{ renderer: "svg" }}
          />
        </Card.Content>
      </Card>
    </div>
  );
};

PieChart.defaultProps = {
  title: "Pie Chart Title",
  styles: {
    height: "500px",
    width: "500px",
    margin: "0 auto"
  },
  legend: ["data1", "data2", "data3", "data4", "data5"],
  data: [
    { value: 1548, name: "data1" },
    { value: 535, name: "data2" },
    { value: 510, name: "data3" },
    { value: 634, name: "data4" },
    { value: 735, name: "data5" }
  ]
};

PieChart.propTypes = {
  title: PropTypes.string,
  styles: PropTypes.object,
  legend: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired
};

export default PieChart;
