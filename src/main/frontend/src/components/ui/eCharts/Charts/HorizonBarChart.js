import React from "react";
import PropTypes from "prop-types";
import { Card } from "semantic-ui-react";
import ReactEcharts from "echarts-for-react";

const HorizonBarChart = props => {
  const getOption = () => ({
    tooltip: {
      trigger: "axis",
      axisPointer: {
        type: "shadow"
      },
      formatter: "{b} ({c})"
    },
    toolbox: {
      feature: {
        saveAsImage: {
          title: "Save As Image",
          pixelRatio: 10,
          emphasis: {
            iconStyle: {
              textAlign: "right"
            }
          }
        }
      }
    },

    yAxis: {
      type: "category",
      data: props.yAxis
    },
    xAxis: {
      type: "value"
    },

    grid: {
      left: "5%",
      right: "5%",
      bottom: "8%",
      containLabel: true
    },
    series: [
      {
        data: props.series,
        type: "bar",
        barMaxWidth: "30px",
        label: {
          normal: {
            show: true,
            position: "insideRight"
          }
        },
        itemStyle: {
          color: "rgba(23, 94, 113, 1)"
        }
      }
    ],
    animationDelay: 1000,
    animationDuration: 2000
  });

  return (
    <Card centered fluid>
      <Card.Content style={{ flex: "none" }}>
        <Card.Header>{props.title}</Card.Header>
      </Card.Content>

      <Card.Content>
        <ReactEcharts
          option={getOption()}
          // style={props.styles}
          className="ReactEcharts"
          //opts={{ renderer: "svg" }}
          style={{
            margin: "0rem auto",
            width: "80%",
            height: "280px",
            padding: "1rem"
            //border: "1px solid black"
          }}
        />
      </Card.Content>
    </Card>
  );
};

HorizonBarChart.defaultProps = {
  title: "Horizon Bar Chart Title",
  yAxis: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
  series: [120, 200, 150, 80, 70, 110, 130]
  // styles: {
  //   height: "300px",
  //   margin: "0 auto"
  // }
};

HorizonBarChart.propTypes = {
  title: PropTypes.string,
  styles: PropTypes.object,
  xAxisData: PropTypes.array,
  yAxisData: PropTypes.array
};

export default HorizonBarChart;
