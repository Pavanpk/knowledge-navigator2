import React from "react";
import PropTypes from "prop-types";
import { Card } from "semantic-ui-react";
import ReactEcharts from "echarts-for-react";

// get data from parents component
// <AreaChart data={_data}/>
// AreaChart = () => ( data = {props.data} )

const AreaChart = props => {
  //const [title, styles, xAxisData, yAxisData] = props;

  const getOption = () => ({
    xAxis: {
      type: "category",
      boundaryGap: false, // area chart only // ture -> empty at the end of each side
      data: props.xAxisData
    },

    yAxis: {
      type: "value"
    },
    toolbox: {
      feature: {
        saveAsImage: {
          title: "Save As Image",
          pixelRatio: 10,
          emphasis: {
            iconStyle: {
              textAlign: "right"
            }
          }
        }
      }
    },

    series: [
      {
        data: props.yAxisData,
        itemStyle: { color: "#175E71" },
        type: "line",
        areaStyle: {} // area chart only
      }
    ],
    animationDelay: 1000,
    animationDuration: 2000
  });

  return (
    <div style={{ width: "fit-content", margin: "0 auto" }}>
      <Card centered fluid>
        <Card.Content style={{ flex: "none" }}>
          <Card.Header>{props.title}</Card.Header>
        </Card.Content>

        <Card.Content>
          <ReactEcharts
            option={getOption()}
            style={props.styles}
            className="ReactEcharts"
            //opts={{ renderer: "svg" }}
          />
        </Card.Content>
      </Card>
    </div>
  );
};

//set defult props

AreaChart.defaultProps = {
  title: "Area Chart Title",
  styles: {
    height: "500px",
    width: "700px",
    margin: "0 auto"
  },
  xAxisData: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
  yAxisData: [820, 600, 901, 934, 1290, 1330, 1320]
};

AreaChart.propTypes = {
  title: PropTypes.string,
  styles: PropTypes.object,
  xAxisData: PropTypes.array.isRequired,
  yAxisData: PropTypes.array.isRequired
};

export default AreaChart;
