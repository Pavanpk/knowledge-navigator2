import React from "react";

import { Card } from "semantic-ui-react";
import ReactEcharts from "echarts-for-react";

const Tree = props => {
  const setOption = () => ({
    tooltip: {
      trigger: "item",
      triggerOn: "mousemove",
      formatter: "{b} {c}"
    },
    legend: {
      top: "2%",
      left: "3%",
      orient: "vertical",
      data: [
        {
          name: props.tree_name,
          icon: "circle"
          //icon: 'circle', 'rect', 'roundRect', 'triangle', 'diamond', 'pin', 'arrow', 'none'
        }
      ]
    },
    series: [
      {
        type: "tree",
        name: props.tree_name,
        data: props.tree_data,
        //layout: "radial",

        symbol: "circle",
        //'circle', 'rect', 'roundRect', 'triangle', 'diamond', 'pin', 'arrow', 'none'
        symbolSize: 7,

        initialTreeDepth: 4,

        label: {
          normal: {
            position: "left",
            verticalAlign: "middle",
            distance: 10,
            align: "right",
            color: "#383838",
            fontSize: 12
          },
          emphasis: {
            color: "#175E71"
          }
        },

        leaves: {
          label: {
            normal: {
              position: "right",
              verticalAlign: "middle",
              align: "left",
              color: "#383838"
            },
            emphasis: {
              color: "#175E71"
            }
          }
        },

        expandAndCollapse: true,

        lineStyle: {},
        itemStyle: {},

        animationDuration: 1000,
        animationDurationUpdate: 750
      }
    ]
  });

  return (
    <div style={{ margin: "0 auto" }}>
      <Card centered fluid>
        <Card.Content>
          <Card.Header>{props.title}</Card.Header>
        </Card.Content>

        <Card.Content>
          <ReactEcharts
            option={setOption()}
            notMerge={true}
            lazyUpdate={true}
            className="KEYKEY"
            style={{
              height: "110vh",
              padding: "1rem"
              //border: "1px solid black"
            }}
          />
        </Card.Content>
      </Card>
    </div>
  );
};

Tree.defaultProps = {};
Tree.propTypes = {};
export default Tree;
