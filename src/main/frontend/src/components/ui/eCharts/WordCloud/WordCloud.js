import React from "react";
import ReactWordcloud from "react-wordcloud";
//https://react-wordcloud.netlify.com/

const WordCloud = props => {
  //   function getCallback(callback) {
  //     return function(word, event) {
  //       const isActive = callback !== "onWordMouseOut";
  //       const element = event.target;
  //       const text = select(element);
  //       text
  //         .on("click", () => {
  //           if (isActive) {
  //             window.open(`https://google.com/?q=${word.text}`, "_blank");
  //           }
  //         })
  //         .transition()
  //         .attr("background", "white")
  //         .attr("font-size", isActive ? "300%" : "100%")
  //         .attr("text-decoration", isActive ? "underline" : "none");
  //     };
  //   }

  return (
    <div
      className="WordCloud"
      style={{
        backgroundColor: "#fff",
        height: "500px",
        width: "80%",
        margin: "1rem auto"
      }}
    >
      <ReactWordcloud
        maxWords={props.maxWords}
        options={{
          //Font Styles
          fontFamily: "Lato",
          fontSizes: [10, 80],
          fontStyle: "normal",
          fontWeight: "500",

          //Rotations
          rotations: 0,
          //rotationAngles: [0, 90],

          scale: "log",
          spiral: "archimedean"
        }}
        words={props.words}
        //callback
        callbacks={{
          // getWordColor: ({ value }) => {
          //   if (value > 40) {
          //     return "#383838";
          //   } else if (value > 20) {
          //     return "#13727C";
          //   } else if (value > 15) {
          //     return "#148CA8";
          //   } else if (value > 10) {
          //     return "#28ABCE";
          //   } else if (value > 5) {
          //     return "#4AC1DD";
          //   } else {
          //     return "#9EE4EF";
          //   }
          // }
          getWordColor: item => {
            return item["Item Color"];
          }
        }}
      />
    </div>
  );
};

export default WordCloud;
