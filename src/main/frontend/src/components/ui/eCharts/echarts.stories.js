import React from "react";
import { storiesOf } from "@storybook/react";

import BarChart from "./Charts/BarChart";
import HorizonBarChart from "./Charts/HorizonBarChart";
import AreaChart from "./Charts/AreaChart";
import LineChart from "./Charts/LineChart";
import StackedAreaChart from "./Charts/StackedAreaChart";
import StackedLineChart from "./Charts/StackedLineChart";
import DoughnutChart from "./Charts/DoughnutChart";
import PieChart from "./Charts/PieChart";
import DataSetChart from "./Charts/DataSetChart";
import Treemap from "./Charts/Treemap";
import Tree from "./Charts/Tree";

import WordCloud from "./WordCloud/WordCloud";

storiesOf("Data Visualation | eCharts ", module)
  .addParameters({
    info: { inline: true }
  })

  .add(
    "Bar Chart",
    () => {
      // const _title = "";
      // const _xAxisData = [];
      // const _yAxisData = [];
      // const _styles = {};

      return <BarChart />;
    },
    {
      info: {
        text: `
      ## eCharts
      JSON file -> Data Array -> Deliever by Array to Echarts Components
      stories file (parents file): data, import chart type
      component file (child file): getOtion + call echart component
      ~~~js
      <BarChart
        title={_title}
        xAxisData={_xAxisData}
        yAxisData={_yAxisData}
        styles={_styles}
      />
      ~~~
    `
      }
    }
  )

  .add(
    "Horizon Bar Chart",
    () => {
      return <HorizonBarChart />;
    },
    {
      info: {
        text: `
      ## eCharts
      JSON file -> Data Array -> Deliever by Array to Echarts Components
      stories file (parents file): data, import chart type
      component file (child file): getOtion + call echart component
      ~~~js
      <HorizonBarChart
        title={_title}
        yAxis={_yAxis}
        series={_series}
        styles={_styles}
      />
      ~~~
    `
      }
    }
  )

  .add(
    "Area Chart",
    () => {
      // const _title = "";
      // const _xAxisData = [];
      // const _yAxisData = [];
      // const _styles = {};

      return <AreaChart />;
    },
    {
      info: {
        text: `
      ## eCharts
      JSON file -> Data Array -> Deliever by Array to Echarts Components
      stories file (parents file): data, import chart type
      component file (child file): getOtion + call echart component
      ~~~js
      <AreaChart
        title={_title}
        xAxisData={_xAxisData}
        yAxisData={_yAxisData}
        styles={_styles}
      />
      ~~~
    `
      }
    }
  )

  .add(
    "Line Chart",
    () => {
      // const _title = "";
      // const _xAxisData = [];
      // const _yAxisData = [];
      // const _styles = {};

      return <LineChart />;
    },
    {
      info: {
        text: `
      ## eCharts
      JSON file -> Data Array -> Deliever by Array to Echarts Components
      stories file (parents file): data, import chart type
      component file (child file): getOtion + call echart component
      ~~~js
      <LineChart
        title={_title}
        xAxisData={_xAxisData}
        yAxisData={_yAxisData}
        styles={_styles}
      />
      ~~~
    `
      }
    }
  )

  .add(
    "Stacked Area Chart",
    () => {
      // const _title = "";
      // const _legend = ["", "", ""];
      // const _xAxisData = ["", "", ""];
      // const _series = [
      //   {
      //     name: "string",
      //     type: "line",
      //     stack: "total",
      //     areaStyle: {},
      //     data: [100, 100, 100]
      //   },
      //   {},
      //   {}
      // ];
      // const _styles = {};

      return <StackedAreaChart />;
    },
    {
      info: {
        text: `
      ## eCharts
      JSON file -> Data Array -> Deliever by Array to Echarts Components
      stories file (parents file): data, import chart type
      component file (child file): getOtion + call echart component
      ~~~js
      <StackedAreaChart
        title={_title}
        xAxisData={_xAxisData}
        legend={_legend}
        yAxisData={_yAxisData}
        series={_series}
        styles={_styles}
      />
      ~~~
    `
      }
    }
  )
  .add(
    "Stacked Line Chart",
    () => {
      // const _title = "";
      // const _legend = ["", "", ""];
      // const _xAxisData = ["", "", ""];
      // const _series = [
      //   {
      //     name: "string",
      //     type: "line",
      //     stack: "total",
      //     data: [100, 100, 100]
      //   },
      //   {},
      //   {}
      // ];
      // const _styles = {};

      return <StackedLineChart />;
    },
    {
      info: {
        text: `
      ## eCharts
      JSON file -> Data Array -> Deliever by Array to Echarts Components
      stories file (parents file): data, import chart type
      component file (child file): getOtion + call echart component
      ~~~js
      <StackedLineChart
        title={_title}
        xAxisData={_xAxisData}
        legend={_legend}
        yAxisData={_yAxisData}
        series={_series}
        styles={_styles}
      />
      ~~~
    `
      }
    }
  )

  .add(
    "DoughnutChart",
    () => {
      // const _title = "";
      // const _styles = {};
      // const _legend = ["", "", ""];
      // const _data = [{ name: "string", value: "number" }, {}, {}];

      return <DoughnutChart />;
    },
    {
      info: {
        text: `
      ## eCharts
      JSON file -> Data Array -> Deliever by Array to Echarts Components
      stories file (parents file): data, import chart type
      component file (child file): getOtion + call echart component
      ~~~js
      <DoughnutChart
        title={_title}
        styles={_styles}
        legend={_legend}
        data={_data}
      />
      ~~~
    `
      }
    }
  )

  .add(
    "Basic Pie Chart",
    () => {
      // const _title = "";
      // const _styles = {};
      // const _legend = ["", "", ""];
      // const _data = [{ name: "string", value: "number" }, {}, {}];

      return <PieChart />;
    },
    {
      info: {
        text: `
      ## eCharts
      JSON file -> Data Array -> Deliever by Array to Echarts Components
      stories file (parents file): data, import chart type
      component file (child file): getOtion + call echart component
      ~~~js
      <PieChart
        title={_title}
        styles={_styles}
        legend={_legend}
        data={_data}
      />
      ~~~
    `
      }
    }
  )

  .add(
    "Data Set Chart",
    () => {
      // const _title = "";
      // const _styles = {};
      // const _dementions = ["col 1 label", "col 2 label", "col 3 label"];
      // const _source = [[], [], []];

      return <DataSetChart />;
    },
    {
      info: {
        text: `
      ## eCharts
      JSON file -> Data Array -> Deliever by Array to Echarts Components
      stories file (parents file): data, import chart type
      component file (child file): getOtion + call echart component
      ~~~js
      <DataSetChart
        title={_title}
        styles={_styles}
        dementions={_dementions}
        source={_source}
      />
      ~~~
    `
      }
    }
  )

  .add(
    "Treemap",
    () => {
      return <Treemap />;
    },
    {
      info: {
        text: `
      ## eCharts
      JSON file -> Data Array -> Deliever by Array to Echarts Components
      stories file (parents file): data, import chart type
      component file (child file): getOtion + call echart component
      ~~~js
      <Treemap
        title={_title}
        styles={_styles}
        dementions={_dementions}
        source={_source}
      />
      ~~~
    `
      }
    }
  )
  .add(
    "Tree",
    () => {
      return <Tree />;
    },
    {
      info: {
        text: `
      ## eCharts
      Tree
      ~~~js
      <Tree
        null
      />
      ~~~
    `
      }
    }
  )

  .add("WordCloud", () => {
    return (
      <div>
        <WordCloud />
      </div>
    );
  });
