import React, { useState } from "react";

import { Sidebar, Menu, Segment } from "semantic-ui-react";
// Aux from "../../../hoc/React.Fragmentilitary/React.Fragmentilitary";

import SidebarContents from "./SidebarContents/SidebarContents";
import AppNavigation from "./AppNavigation/AppNavigation";

import "./LayoutMenu.scss";

const LayoutMenu = props => {
  const [dimmed] = useState(true);

  const [visible, setVisible] = useState(false);
  const visibleHandler = () => {
    setVisible(!visible);
  };

  const [topic, setTopic] = useState("CBD");
  const onChangeTopic = newTopic => {
    setTopic(newTopic);
  };

  return (
    <React.Fragment>
      <AppNavigation
        isAuthenticated={props.isAuth}
        //changed name -> prevent confused props
        visible={visible}
        click={visibleHandler}
        topic={topic}
        username={props.username}
        {...props}
      />

      <Sidebar.Pushable as={Segment} style={{ border: "none" }}>
        <Sidebar
          as={Menu}
          animation="overlay"
          // animation: overlay, push, scale down, uncover, slide out, slide along
          direction="left"
          icon="labeled"
          vertical
          visible={visible}
          width="wide"
          className="Sidebar"
        >
          <SidebarContents
            topic={topic}
            change={onChangeTopic}
            username="Knowledge Navigator"
          />
        </Sidebar>

        <Sidebar.Pusher
          dimmed={dimmed && visible}
          onClick={visible ? visibleHandler : null}
        >
          {props.children}
        </Sidebar.Pusher>
      </Sidebar.Pushable>
    </React.Fragment>
  );
};

export default LayoutMenu;

LayoutMenu.propTypes = {
  //  visible: PropTypes.bool
};
