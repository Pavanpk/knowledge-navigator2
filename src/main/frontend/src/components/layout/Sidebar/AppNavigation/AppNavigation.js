import React, { useState } from "react";

import AuthDropDown from "./AuthoDropDown/AuthDropDown";
import { Menu, Visibility, Container, Button } from "semantic-ui-react";
import "./AppNavigation.scss";
import { withRouter, NavLink } from "react-router-dom";

import Hamburger from "../../../ui/Hamburger/Hamburger";

const AppNavigation = props => {
  const [menuFixed, setMenuFixed] = useState(false);
  const [activeItem] = useState(null);

  const stickTopMenu = () => {
    setMenuFixed(true);
  };

  const unStickTopMenu = () => {
    setMenuFixed(false);
  };

  const menuStyle = {
    border: "none",
    borderRadius: 0,
    boxShadow: "none",
    marginBottom: "1em",
    marginTop: "1em",
    transition: "box-shadow 0.5s ease, padding 0.5s ease"
  };

  const fixedMenuStyle = {
    backgroundColor: "#fff",
    border: "1px solid #ddd",
    boxShadow: "0px 3px 5px rgba(0, 0, 0, 0.2)"
  };

  const pathItem = [
    "webanalytics",
    "results",
    "sentiments",
    "influencers",
    "demographics",
    "new"
  ];

  const pathMenuItem = pathItem.map(item => {
    return (
      <NavLink to={"/" + item} activeClassName="active" key={item}>
        <Menu.Item
          header
          name={item.charAt(0).toUpperCase() + item.slice(1)}
          active={activeItem === item}
        />
      </NavLink>
    );
  });

  return (
    <Visibility
      onBottomPassed={stickTopMenu}
      onBottomVisible={unStickTopMenu}
      once={false}
    >
      <Menu
        borderless
        fixed={menuFixed ? "top" : undefined}
        style={menuFixed ? fixedMenuStyle : menuStyle}
      >
        <Container
          fluid
          style={{ padding: "1rem 2rem" }}
          className="NaviContainer"
        >
          <Menu.Item>
            <Hamburger visible={props.visible} click={props.click} />
          </Menu.Item>

          {pathMenuItem}

          <Menu.Menu position="right">
            <Button compact basic floated="right">
              {props.topic}
            </Button>

            {!props.isAuthenticated ? ( // login: false
              <AuthDropDown username="Guest" {...props} />
            ) : (
              <AuthDropDown username={props.username} {...props} />
            )}
          </Menu.Menu>
        </Container>
      </Menu>
    </Visibility>
  );
};

export default withRouter(AppNavigation);
