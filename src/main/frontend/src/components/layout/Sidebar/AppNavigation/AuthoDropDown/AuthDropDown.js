import React from "react";
import { Dropdown } from "semantic-ui-react";
// import { withRouter } from "react-router-dom";
// import { connect } from "react-redux";
// import * as actions from "../../../../../store/actions/index";

const AuthDropDown = props => {
  //let username = props.username;

  return (
    //<Menu.Menu position="right">
    <Dropdown
      //text={username}
      text="FFM"
      pointing
      className="link item"
    >
      <Dropdown.Menu>
        <Dropdown.Item as="a" href="/" text="Profile" />
        <Dropdown.Item text="Logout" onClick={props.Onlogout} />
      </Dropdown.Menu>
    </Dropdown>
    //</Menu.Menu>
  );
};

// const mapDispatchToProps = dispatch => {
//   return {
//     Onlogout: () => dispatch(actions.logout())
//   };
// };

// export default withRouter(
//   connect(
//     null,
//     mapDispatchToProps
//   )(AuthDropDown)
// );

export default AuthDropDown;
