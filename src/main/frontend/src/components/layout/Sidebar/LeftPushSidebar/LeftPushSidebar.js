import React, { Component } from "react";
import PropTypes from "prop-types";
import { Sidebar, Menu, Segment } from "semantic-ui-react";
// Aux from "../../../../hoc/React.Fragmentilitary/React.Fragmentilitary";

import SidebarContents from "../SidebarContents/SidebarContents";
import AppNavigation from "../AppNavigation/AppNavigation";

import "./LeftPushSidebar.scss";

const VerticalSidebar = props => {
  return (
    <Sidebar
      as={Menu}
      animation={props.animation}
      direction={props.direction}
      icon="labeled"
      vertical
      visible={props.visible}
      width="wide"
      className="Sidebar"
    >
      <SidebarContents topic={props.topic} username="Knowledge Navigator" />
    </Sidebar>
  );
};

VerticalSidebar.propTypes = {
  visible: PropTypes.bool
};

class LeftPushSidebar extends Component {
  state = {
    animation: "overlay",
    direction: "left",
    dimmed: true,
    visible: false,
    topic: "CBD"
  };

  render() {
    const VisibleHandler = () => {
      this.setState({ visible: !this.state.visible });
    };
    return (
      <React.Fragment>
        <AppNavigation
          visible={this.state.visible}
          click={VisibleHandler}
          topic={this.state.topic}
        />

        <Sidebar.Pushable as={Segment} style={{ border: "none" }}>
          <VerticalSidebar
            animation={this.state.animation}
            direction={this.state.direction}
            visible={this.state.visible}
            topic={this.state.topic}
          />
          <Sidebar.Pusher
            dimmed={this.state.dimmed && this.state.visible}
            onClick={this.state.visible ? VisibleHandler : null}
          >
            {this.props.children}
          </Sidebar.Pusher>
        </Sidebar.Pushable>
      </React.Fragment>
    );
  }
}

export default LeftPushSidebar;
