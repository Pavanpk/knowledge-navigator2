import React, { Component } from "react";
import { Header, Image, Form, Checkbox, Container } from "semantic-ui-react";

const TopicGroup = props => {
  const list = [
    "Connabidiol",
    "Pets",
    "Sports",
    "Foods/Beverages",
    "Skin Care",
    "Medical"
  ];
  const ListGroup = list.map(item => {
    return (
      <div key={item}>
        <Form.Field className="CheckBoxArea">
          <Checkbox
            radio
            label={item}
            value={item}
            checked={props.topic === item} // not {item}
            onChange={() => props.change(item)}
            //change로 전달 받은 함수를 여기에서 실행!
            //전달할 매개 변수를 괄호 안에 넣기
          />
        </Form.Field>
      </div>
    );
  });

  return (
    <Form className="Form">
      <Form.Field>
        Selected value: <b>{props.topic}</b>
      </Form.Field>

      {ListGroup}
    </Form>
  );
};

class SidebarContents extends Component {
  render() {
    return (
      <div className="sidebarContents">
        <Container style={{ marginTop: "40px" }}>
          <Header as="h3" textAlign="center">
            <Image
              className="AddShadow"
              src="https://picsum.photos/200"
              avatar
            />
            <Header.Content>{this.props.username}</Header.Content>
          </Header>

          {/* <Input icon="search" fluid placeholder="Search..." />
          <Divider section /> */}

          <div className="TextContainer">
            <p>
              one topic keyword below and press apply to load all results for
              that topic
            </p>
          </div>

          <div className="KeywordBox">
            <h5 style={{ color: "#383838" }}>Topic Key Words</h5>
            <TopicGroup topic={this.props.topic} change={this.props.change} />
            {/* 
            <Button primary>Apply</Button>
            <Button>Reset</Button> */}
          </div>
        </Container>
      </div>
    );
  }
}

export default SidebarContents;
