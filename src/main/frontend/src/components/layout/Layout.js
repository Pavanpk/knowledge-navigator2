import React from "react";
import { Container } from "semantic-ui-react";

import AppFooter from "./AppFooter/AppFooter";
import AppHeader from "./AppHeader/AppHeader";
import LayoutMenu from "./Sidebar/LayoutMenu";

//import firebase from "../../firebase";

const Layout = props => {
  const username = "Jongsun Park";

  return (
    <React.Fragment>
      <AppHeader />
      <LayoutMenu
        isAuth={props.isAuthenticated}
        username={username}
        logout={() => {}}
        {...props}
      >
        <Container style={{ maxWidth: "1200px" }}>{props.children}</Container>
      </LayoutMenu>
      <AppFooter />
    </React.Fragment>
  );
};

export default Layout;
