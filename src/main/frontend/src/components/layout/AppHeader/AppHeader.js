import React from "react";
import { Container, Image } from "semantic-ui-react";
import Logo from "../../../assets/images/kn_logo.png";

class AppHeader extends React.Component {
  state = {};
  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  render() {
    return (
      <Container fluid textAlign="center">
        <Image
          src={Logo}
          className="Logo"
          style={{ width: "20%", minWidth: "300px", margin: "2rem" }}
        />
      </Container>
    );
  }
}

export default AppHeader;
