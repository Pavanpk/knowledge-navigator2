import React from 'react';
import './vendors/jquery/dist/jquery.min.js'
import './vendors/popper.js/dist/umd/popper.min'
import 'bootstrap/dist/js/bootstrap.min.js'
import './dist/js/jquery.slimscroll.js'
import './dist/js/feather.min.js'
import './dist/js/init.js'

class NotFound extends React.Component {
    render() {
        return (
            <div>

                <div className="preloader-it">
                    <div className="loader-pendulums"/>
                </div>

                <div className="hk-wrapper">

                    <div className="hk-pg-wrapper hk-auth-wrapper">
                        <header className="d-flex justify-content-end align-items-center">
                            <div className="btn-group btn-group-sm">
                                <a href="#" className="btn btn-outline-secondary">Help</a>
                                <a href="#" className="btn btn-outline-secondary">About Us</a>
                            </div>
                        </header>
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-xl-12 pa-0">
                                    <div className="auth-form-wrap pt-xl-0 pt-70">
                                        <div className="auth-form w-xl-25 w-sm-50 w-100">
                                            <a className="auth-brand text-center d-block mb-45" href="#">
                                                <img className="brand-img" src="../dist/img/logo-light.png"
                                                     alt="brand"/>
                                            </a>
                                            <form>
                                                <h1 className="display-4 mb-10 text-center">404. That's an error.</h1>
                                                <p className="mb-30 text-center">We are sorry but requested page does
                                                    not
                                                    exist. You can <a href="dashboard1.html"><u>return to
                                                        homepage</u></a> or make search below to find the correct page.
                                                </p>
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <input
                                                            className="form-control form-control-lg filled-input rounded-input bg-white"
                                                            placeholder="Search and hit Enter" type="text"/>
                                                        <div className="input-group-append">
                                                        <span className="input-group-text"><span
                                                            className="feather-icon"><i
                                                            data-feather="arrow-right"/></span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

export default NotFound;



