import React from "react";

import "./App.scss";
import "semantic-ui-css/semantic.min.css";

import { Route, withRouter, Redirect } from "react-router-dom";

import { Trend } from "./components/trend/Trend";

const App = () => {
  return (
      <Trend></Trend>
    /*<React.Fragment>
      <Redirect to="/app" />
      <Route path="/app" component={Trend} />
    </React.Fragment>*/
  );
};

export default withRouter(App);
