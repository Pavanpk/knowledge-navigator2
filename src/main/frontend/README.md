This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `yarn build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

## Semantic - UI

### documentation

official (https://react.semantic-ui.com/)

### install

\$ yarn add semantic-ui-react semantic-ui-css
(after install you can find dependency in 'package.json')

### usage

CSS: (in index.js / app.js / config.js)
import 'semantic-ui-css/semantic.min.css'

Component:
import { Button } from 'semantic-ui-react'
<Button>Click Here</Button>

## eChart

### documentation

offical eChart Apache (https://echarts.apache.org/)
eChart for React (https://www.npmjs.com/package/echarts-for-react)

### install

\$ yarn add echarts-for-react

### usage

import ReactEcharts from 'echarts-for-react';
const option = {
// example (https://echarts.apache.org/examples/en/editor.html?c=line-simple)
// options ( xAix: {}, yAxis: {}, series: {} )
}
<ReactEcharts option={option} />

## Router

## Redux
